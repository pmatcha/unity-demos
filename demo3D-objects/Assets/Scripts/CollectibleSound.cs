﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleSound : MonoBehaviour
{
    [SerializeField] private AudioClip pickupSound;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnDestroy()
    {
        AudioSource.PlayClipAtPoint(pickupSound, transform.position);
    }
}
