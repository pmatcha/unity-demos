﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAccessor : MonoBehaviour
{

    public static UIAccessor Instance;

    void Awake()
    {
        Instance = this;
    }

    public void OpenDialogue(DialogueObj doo)
    {
        DialogueManager.Instance.ApplyDialogue(doo);
    }

    public void InteractPromptStatus(bool on)
    {
        CanvasInstance.Instance.interactPrompt.SetActive(on);
    }
}
