﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEvent : MonoBehaviour
{
    public string sound;

    public void Play()
    {
        AudioManager.Instance.Play(sound);
    }

    public void Stop()
    {
        AudioManager.Instance.Stop(sound);
    }
}
