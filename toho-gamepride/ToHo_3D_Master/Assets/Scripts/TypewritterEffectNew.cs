﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TypewritterEffectNew : MonoBehaviour
{
    [Range(0.01f, 0.1f)]
    public float m_characterInterval; //(in secs)
    public bool reset;

    private string m_partialText;
    private float m_cumulativeDeltaTime;


    private Text m_label;

    void Awake()
    {
        m_label = GetComponent<Text>();
    }

    void Start()
    {
        m_partialText = "";
        m_cumulativeDeltaTime = 0;
    }

    void Update()
    {
        if(reset)
        {
            m_cumulativeDeltaTime = 0;
            reset = false;
        }

        m_cumulativeDeltaTime += Time.deltaTime;
        while (m_cumulativeDeltaTime >= m_characterInterval && m_partialText.Length < DialogueManager.Instance.dialogueText.text.Length)
        {
            m_partialText += DialogueManager.Instance.dialogueText.text[m_partialText.Length];
            m_cumulativeDeltaTime -= m_characterInterval;
        }
        m_label.text = m_partialText;
    }

}
