﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Sound 
{
    [Header("Sound")]
    public string soundName;
    public AudioClip clip;
    public AudioGroup group;
    public bool loop;

    [Header("3D Sound")]
    public bool is3DSound;
    public bool isDymanic;
    public float minDistance;
    public float maxDistance;

    [Range(0f, 1f)]
    public float volume = 1;

    [Range(0.1f, 3f)]
    public float pitch = 1;

    [Header("Sources")]
    public AudioSource source;
}
