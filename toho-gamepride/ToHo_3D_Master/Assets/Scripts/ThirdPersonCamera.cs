﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCamera : MonoBehaviour
{
    [Header("Stats")]
    public float minAngle = -35;
    public float maxAngle = 35;
    public float lookAngle;
    public float tiltAngle;

    [Header("Check States")]
    public bool canRotateCamera;

    [Header("Other")]
    public float followSpeed = 3;
    public float mouseSpeed = 2;
    public Camera mainCamera;
    public Transform target;
    public Transform pivot;
    public float h;
    public float v;

    float turnSmoothing = .1f;

    float smoothX;
    float smoothY;

    float smoothXvelocity;
    float smoothYvelocity;

    public static ThirdPersonCamera Instance;
    
    void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        canRotateCamera = true;
    }

    // Update is called once per frame
    void Update()
    {
        h = Input.GetAxis("Mouse X");
        v = Input.GetAxis("Mouse Y");

        FollowTarget();
        HandleRotations();

    }

    void FollowTarget()
    {
        float speed = Time.deltaTime * followSpeed;

        Vector3 targetPosition = Vector3.Lerp(transform.position, target.position, speed);
        transform.position = targetPosition;

    }

    void HandleRotations()
    {
        if (turnSmoothing > 0)
        {
            smoothX = Mathf.SmoothDamp(smoothX, h, ref smoothXvelocity, turnSmoothing);
            smoothY = Mathf.SmoothDamp(smoothY, v, ref smoothYvelocity, turnSmoothing);
        }
        else
        {
            smoothX = h;
            smoothY = v;
        }

        tiltAngle -= smoothY * mouseSpeed;
        tiltAngle = Mathf.Clamp(tiltAngle, minAngle, maxAngle);
        pivot.localRotation = Quaternion.Euler(tiltAngle, 0, 0);
        lookAngle += smoothX * mouseSpeed;
        transform.rotation = Quaternion.Euler(0, lookAngle, 0);

    }
}
