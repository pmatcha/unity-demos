﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressAccessor : MonoBehaviour
{
    public void Apply()
    {
        SavePlayerProgress.Instance.Apply();
    }

    public void GetLastPlayerPosition()
    {
        SavePlayerProgress.Instance.GetLastPlayerPosition();
    }

    public void GetLastHealth()
    {
        SavePlayerProgress.Instance.GetLastHealth();
    }

    public void EnableFlour()
    {
        SavePlayerProgress.Instance.EnableFlour();
    }

    public void EnableEgg()
    {
        SavePlayerProgress.Instance.EnableEgg();
    }

    public void EnableSugar()
    {
        SavePlayerProgress.Instance.EnableSugar();
    }

    public void EnableCocoa()
    {
        SavePlayerProgress.Instance.EnableCocoa();
    }
}
