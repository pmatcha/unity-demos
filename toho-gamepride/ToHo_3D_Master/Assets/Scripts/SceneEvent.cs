﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneEvent : MonoBehaviour
{
    public void LoadLevel(string s)
    {
        MySceneManager.Instance.LoadScene(s);
    }

    public void UnloadLevel(string s)
    {
        MySceneManager.Instance.UnloadScene(s);
    }

    public void ReloadCurrentLevel()
    {
        MySceneManager.Instance.ReloadCurrentLevel();
    }

    public void UnloadCurrentScene()
    {
        MySceneManager.Instance.UnloadCurrentScene();
    }

    public void LoadNextScene()
    {
        UnloadCurrentScene();
        MySceneManager.Instance.LoadNextScene();
    }
}
