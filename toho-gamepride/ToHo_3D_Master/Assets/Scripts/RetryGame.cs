﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RetryGame : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            MySceneManager.Instance.ReloadCurrentLevel();
            CanvasInstance.Instance.gameOverScreen.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.Q))
            Application.Quit();
    }
}
