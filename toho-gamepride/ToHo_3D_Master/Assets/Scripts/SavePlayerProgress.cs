﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavePlayerProgress : MonoBehaviour
{
    public float lastHealth;
    public Vector3 lastPos;
    public bool hasFlour;
    public bool hasEgg;
    public bool hasSugar;
    public bool hasCocoa;
    private bool hasAll;
    public string finalScene;
    public string[] vnToLoad;
    public int sceneIndex;

    public static SavePlayerProgress Instance;
    void Awake()
    {
        Instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        if(hasFlour && hasEgg && hasSugar && hasCocoa)
        {
            if(!hasAll)
            {
                MySceneManager.Instance.LoadAndUnloadScenes(finalScene, MySceneManager.Instance.currentScene.sceneId);
                hasAll = true;
            }
        }
    }

    public void Apply()
    {
        PlayerCharacter.Instance.transform.position = lastPos;
        PlayerCharacter.Instance.health = lastHealth;
    }

    public void IncreaseSceneIndex()
    {
        sceneIndex++;
    }

    public void LoadCurrentScene()
    {
        MySceneManager.Instance.LoadAndUnloadScenes(vnToLoad[sceneIndex], MySceneManager.Instance.currentScene.sceneId);
    }

    public void GetLastPlayerPosition()
    {
        lastPos = PlayerCharacter.Instance.transform.position;
    }

    public void GetLastHealth()
    {
        lastHealth = PlayerCharacter.Instance.health;
    }

    public void EnableFlour()
    {
        hasFlour = true;
    }

    public void EnableEgg()
    {
        hasEgg = true;
    }

    public void EnableSugar()
    {
        hasSugar = true;
    }

    public void EnableCocoa()
    {
        hasCocoa = true;
    }
}
