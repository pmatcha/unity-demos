﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SceneData 
{
    public string sceneId;
    public bool isLoaded;
    public string nextScene;
}
