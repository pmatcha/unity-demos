﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class UnityEventDelay 
{
    [Header("Events")]
    public UnityEvent e;
    public bool eventPlaying;
    public float timer;
    public float delay;
    public bool lastInOrder;
}

