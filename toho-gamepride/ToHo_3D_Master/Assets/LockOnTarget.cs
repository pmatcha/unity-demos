﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockOnTarget : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (CheckDistance() < LockOnManager.Instance.distance)
        {
            if (!LockOnManager.Instance.avaiableTargets.Contains(this))
                LockOnManager.Instance.avaiableTargets.Add(this);
        }
        else
        {
            if (LockOnManager.Instance.avaiableTargets.Contains(this) &&
                this.transform.position == PlayerCharacter.Instance.m.target.transform.position)
            {
                LockOnManager.Instance.avaiableTargets.Remove(this);
            }

        }
        /*else
        {
            if (LockOnManager.Instance.avaiableTargets.Contains(this))
            {
                if(this.gameObject == PlayerCharacter.Instance.m.target)
                {
                    LockOnManager.Instance.RemoveTarget();
                    LockOnManager.Instance.avaiableTargets.Remove(this);
                }
                else
                    LockOnManager.Instance.avaiableTargets.Remove(this);
            }
        }*/
    }

    float CheckDistance()
    {
        float rDis = Vector3.Distance(PlayerCharacter.Instance.transform.position, this.transform.position);
        return rDis;
    }

    public void Remove()
    {
        if (LockOnManager.Instance.avaiableTargets.Contains(this))
            LockOnManager.Instance.avaiableTargets.Remove(this);

        if (LockOnManager.Instance.targets.Contains(this))
            LockOnManager.Instance.targets.Remove(this);
    }
}
