﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChanger : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.Operate();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Operate()
    {
        Color random = new Color(Random.Range(0f,1f),
        Random.Range(0f,1f), Random.Range(0f,1f));
        GetComponent<Renderer>().material.color = random;
    }
}
