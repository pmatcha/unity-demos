﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour, IGameManager
{

    public ManagerStatus status {get; private set;}
    // Add volume controls here (listing 11.4)

    [SerializeField] private AudioSource music1Source;
    [SerializeField] private AudioClip levelBGMusic;
    public void Startup()
    {
        Debug.Log("Audio manager starting...");
        // Initialize music sources here (listing 11.11)
        PlayLevelMusic();
        status = ManagerStatus.Started;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void PlayLevelMusic() {
        PlayMusic(levelBGMusic);
    }
    private void PlayMusic(AudioClip clip)
    {
        music1Source.clip = clip;
         Debug.Log("Playing " + clip.ToString());
        music1Source.Play();
    }
    
    public void StopMusic()
    {
        music1Source.Stop();
    }
}