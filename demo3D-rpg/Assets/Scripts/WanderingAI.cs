﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WanderingAI : MonoBehaviour
{

    public float speed = 3.0f;
    public float obstacleRange = 5.0f;
    private bool _alive;
    [SerializeField] private GameObject fireballPrefab;
    private GameObject _fireball;

    // Start is called before the first frame update
    void Start()
    {
        _alive = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (_alive) 
        {
            transform.Translate(0, 0, speed * Time.deltaTime);

            Ray ray = new Ray(transform.position, transform.forward);
            RaycastHit hit;
            if (Physics.SphereCast(ray, 0.75f, out hit))
            {
                GameObject hitObject = hit.transform.gameObject;
                if (hitObject.name.Contains("Wall") |
                    hitObject.name.Contains("Door") |
                    hitObject.name.Contains("Platform"))
                {
                    if (hit.distance < obstacleRange)
                    {
                        float angle = Random.Range(-110, 110);
                        transform.Rotate(0, angle, 0);
                    }
                }
            }
            Collider[] hitColliders = Physics.OverlapSphere(transform.position, 10f);
            foreach (Collider hitCollider in hitColliders)
            {
                Vector3 directionToPlayer = hitCollider.transform.position - transform.position;
                directionToPlayer.Set(directionToPlayer.x, (directionToPlayer.y + 0.75f), directionToPlayer.z);
                if (Vector3.Dot(transform.forward, directionToPlayer) > 0.99f)
                {
                    if (hitCollider.GetComponent<PlayerCharacter>())
                    {
                        if (_fireball == null)
                        {
                            _fireball = Instantiate(fireballPrefab) as GameObject;
                            Quaternion fireballRotation = Quaternion.LookRotation(directionToPlayer, Vector3.up);
                            _fireball.transform.position = transform.TransformPoint(transform.forward * 1.5f);
                            _fireball.transform.rotation = fireballRotation;
                        }
                    }
                }
            }
        }
    }

    public void SetAlive(bool alive) {
        _alive = alive;
    }
}
