﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageDealer : MonoBehaviour
{
    public int baseDamage = 1;
    [SerializeField] private Animator _characterAnimator;
    bool isAttacking = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (_characterAnimator.GetCurrentAnimatorClipInfo(0)[0].clip.name.Contains("Attack"))
        {
            isAttacking = true;
            
        }
        else
        {
            isAttacking = false;
        }
    }

    void OnTriggerEnter(Collider hit)
    {
        HitReceiver hitReceiver = hit.gameObject.GetComponent<HitReceiver>();
        if ((hitReceiver != null) && isAttacking)
        {
            hitReceiver.getHit(baseDamage);
        }
    }
}
