﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitReceiver : MonoBehaviour
{
    int totalHP = 1;
    int remainingHP;
    // Start is called before the first frame update
    void Start()
    {
        remainingHP = totalHP;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public int getHit(int damageAmount)
    {
        if (remainingHP > 0)
        {
            remainingHP -= damageAmount;
            Debug.Log(this.gameObject.ToString() + " damaged! remaining hp: " + remainingHP);
            this.GetComponentInParent<DestroyedBehaviour>().Die();
        }
        return 0;
    }
}
