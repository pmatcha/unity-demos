﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyedBehaviour : MonoBehaviour
{

    [SerializeField] private GameObject spawn;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Die()
    {
        GameObject brokenCrate = Instantiate(spawn) as GameObject;
        brokenCrate.transform.position = new Vector3(transform.position.x, transform.position.y-0.5f, transform.position.z);
        brokenCrate.transform.rotation = transform.rotation;
        Destroy(gameObject);
        Debug.Log("killed");
    }
}
