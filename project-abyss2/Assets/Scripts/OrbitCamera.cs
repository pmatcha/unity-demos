﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitCamera : MonoBehaviour
{
    [SerializeField] private Transform target;

    public float rotSpeed = 1.5f;
    private float _rotY;
    private float _rotX;
    private float _previousRotX;
    private Vector3 _offset;
    private Vector3 _tempOffset;
    public float MaxTargetCameraDiffY;
    public string nameOfPlayerObjectToIgnoreRaycast;
    // Start is called before the first frame update
    void Start()
    {
        _rotY = transform.eulerAngles.y;
        _rotX = 0;
        _offset = target.position - transform.position;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("escape") && Cursor.visible)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            Debug.Log("Locking cursor");
        }
        else if (Input.GetKeyDown("escape") && !Cursor.visible)
        {   
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            Debug.Log("Unlocking cursor");
        }
    }

    void LateUpdate()
    {
        float horInput = Input.GetAxis("Horizontal");
        //if (horInput != 0)
        //{
            //_rotY += horInput * rotSpeed;
        //}
        //else
        //{
            _rotY += Input.GetAxis("Mouse X") * rotSpeed * 2;
            _previousRotX = _rotX;
            _rotX += Input.GetAxis("Mouse Y") * rotSpeed * 2;
        //}
        
        Quaternion rotation = Quaternion.Euler(-_rotX, _rotY, 0);
        float targetCameraDiffY = (((target.position - (rotation * _offset)).y) - (target.position.y));
        if (targetCameraDiffY > MaxTargetCameraDiffY || targetCameraDiffY < -MaxTargetCameraDiffY)
        {
            _rotX = _previousRotX;
            rotation = Quaternion.Euler(-_rotX, _rotY, 0);
        }

        //Debug.Log(targetCameraDiffY);

        if (targetCameraDiffY < -0)
        {
            _tempOffset = _offset.normalized;
            //Debug.Log("Offset: " + _offset);
            //Debug.Log("Offset Magnitude: " + _offset.magnitude);
            //Debug.Log("Offset Normalized: " + _offset.normalized);
            //Debug.Log("New Magnitude: " + (((_offset.magnitude)/-targetCameraDiffY)));
            float _tempOffsetMagnitude = Mathf.Clamp(((_offset.magnitude)/-(targetCameraDiffY)), 1f, _offset.magnitude);
            _tempOffset = _tempOffset * _tempOffsetMagnitude;
        }
        else
        {
            _tempOffset = _offset;
        }
        
        //Debug.Log(target.GetComponentInParent<CharacterController>().radius);
        transform.position = target.position - (rotation * _tempOffset);
        transform.LookAt(target);

        RaycastHit hit;
        /* while (isClipping)
        {
            if (Physics.Raycast(transform.position, transform.forward, out hit))
            {
                if (hit.collider.gameObject.ToString().Contains("orin"))
                {
                    //Nothing inbetween, everything correct!
                    isClipping = false;
                }
                else
                {
                    //Camera is clipping
                    isClipping = true;
                    Vector3 _closerTempOffset = _tempOffset.normalized;

                    if (Physics.Raycast(target.position, -transform.forward, out hit))
                    {
                        if (hit.distance < 0)
                        {
                            break;
                        }
                        float _closerTempOffsetMagnitude = Mathf.Clamp(hit.distance, 0f, _tempOffset.magnitude);
                        _closerTempOffset = _closerTempOffset*_closerTempOffsetMagnitude;

                        transform.position = target.position - (rotation * _closerTempOffset);
                        transform.LookAt(target);
                    }
                    else
                    {
                        Debug.Log("Something went wrong with the camera collision code!");
                        break;
                    }
                }
            }
            else
            {
                isClipping = false;
            }
        } */

        int architectureMask = LayerMask.GetMask("Architecture");
        //ignoreClippingMask = ~ignoreClippingMask;

        if (Physics.Raycast(target.position, -transform.forward, out hit, 999f, architectureMask))
        {
            if (hit.collider.gameObject.ToString().Contains(nameOfPlayerObjectToIgnoreRaycast) == false)
            {
                //Debug.Log(hit.collider.gameObject.ToString());

                Vector3 _closerTempOffset = _tempOffset.normalized;

                float _closerTempOffsetMagnitude = Mathf.Clamp(hit.distance, 0f, _tempOffset.magnitude);
                _closerTempOffset = _closerTempOffset*_closerTempOffsetMagnitude;

                transform.position = target.position - (rotation * _closerTempOffset);
                transform.LookAt(target);
            }
        }

    }
}
