﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponEquip : MonoBehaviour
{

    [SerializeField] private Transform handBone;

    // Start is called before the first frame update
    void Start()
    {
        this.transform.parent = handBone;
        this.transform.localPosition = Vector3.zero;
        //this.transform.localRotation = Quaternion.identity;
        //this.transform.localScale = Vector3.one;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
