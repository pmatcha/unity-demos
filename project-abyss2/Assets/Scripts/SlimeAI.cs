﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeAI : MonoBehaviour
{
    
    private Animator _animator;
    private bool _isDead = false;
    public float moveSpeed = 10f;
    public float distanceToDetectTarget = 2f;

    [SerializeField] private GameObject target;
    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 movement = Vector3.zero;
        
        
        if (_isDead == false && Vector3.Distance(transform.position, target.transform.position) < distanceToDetectTarget)
        {
            float step = moveSpeed * Time.deltaTime;
            
            Vector3 oldPosition = transform.position;

            transform.position = Vector3.MoveTowards(transform.position, target.transform.position, step);

            Vector3 newPosition = transform.position;
            Vector3 directionOfMovement = newPosition - oldPosition;
            directionOfMovement.y = 0;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(directionOfMovement), 0.15f);
            
            _animator.SetFloat("Speed", step);
        }
        else 
        {
                _animator.SetFloat("Speed", 0f);
        }

    }

    public void setTarget(GameObject incomingTarget)
    {
        target = incomingTarget;
    }

    public void stop()
    {
        _isDead = true;
    }
}