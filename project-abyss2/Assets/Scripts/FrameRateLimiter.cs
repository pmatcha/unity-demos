﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrameRateLimiter : MonoBehaviour
{         
     public int desiredFPS = 60;
 
     void Awake()
     {
         Application.targetFrameRate = -1;
         QualitySettings.vSyncCount = 0;
     }
 
     void Update()
     {
         long lastTicks = System.DateTime.Now.Ticks;
         long currentTicks = lastTicks;
         float delay = 1f / desiredFPS;
         float elapsedTime;
 
         if (desiredFPS <= 0)
             return;
 
         while (true)
         {
             currentTicks = System.DateTime.Now.Ticks;
             elapsedTime = (float)System.TimeSpan.FromTicks(currentTicks - lastTicks).TotalSeconds;
             if(elapsedTime >= delay)
             {
                 break;
             }
         }
     }
 }