﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestroy : MonoBehaviour
{
    public float timeToLive;
    
    private float _timer = 0;
    public bool _isSelfDestroying = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (_isSelfDestroying)
        {
            _timer += Time.deltaTime;
            if (_timer > timeToLive)
            {
                Debug.Log("destroy!");
                Destroy(gameObject);
            }
        }
    }

    public void startSelfDestruction(bool dewIt)
    {
        _isSelfDestroying = dewIt; // but it's not the jedi way!
    }
}
