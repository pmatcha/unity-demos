﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayShooter : MonoBehaviour
{
    private Camera _camera;

    // Start is called before the first frame update
    void Start()
    {
        _camera = GetComponent<Camera>();

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        
    }

    void OnGUI() {
        int size = 12;
        float posX = _camera.pixelWidth/2 - size/2;
        float posY = _camera.pixelHeight/2 - size;
        GUI.Label(new Rect(posX, posY, size, size), "*");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && Cursor.visible == false)
        {
            Vector3 point = new Vector3(_camera.pixelWidth/2, _camera.
            pixelHeight/2, 0);
            Ray ray = _camera.ScreenPointToRay(point);
            RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    //StartCoroutine(SphereIndicator(hit.point));
                    GameObject hitObject = hit.transform.gameObject;
                    ReactiveTarget target = hitObject.GetComponent<ReactiveTarget>();
                    if (target != null) {
                        target.ReactToHit();
                        Messenger.Broadcast(GameEvent.ENEMY_HIT);
                    } else {
                        StartCoroutine(SphereIndicator(hit.point));
                    }
                }
        }

        if (Input.GetKeyDown("escape"))
        {
            if (Cursor.lockState == CursorLockMode.None)
            {
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
            else
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;   
            }
        }
    }

    private IEnumerator SphereIndicator(Vector3 pos)
    {
        GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        sphere.transform.position = pos;

        yield return new WaitForSeconds(1);

        Destroy(sphere);
    }
}
