﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemRotator : MonoBehaviour
{
    
    [SerializeField] private float rotationSpeed_X;
    [SerializeField] private float rotationSpeed_Y;
    [SerializeField] private float rotationSpeed_Z;
    private float _rotY;
    private float _rotX;
    private float _rotZ;
    // Start is called before the first frame update
    void Start()
    {
        _rotX = transform.eulerAngles.x;
        _rotY = transform.eulerAngles.y;
        _rotZ = transform.eulerAngles.z;
    }

    // Update is called once per frame
    void Update()
    {
        _rotX += rotationSpeed_X * Time.deltaTime;
        _rotY += rotationSpeed_Y * Time.deltaTime;
        _rotZ += rotationSpeed_Z * Time.deltaTime;
        Quaternion rotation = Quaternion.Euler(_rotX, _rotY, _rotZ);
        transform.SetPositionAndRotation(transform.position, rotation);
    }
}
