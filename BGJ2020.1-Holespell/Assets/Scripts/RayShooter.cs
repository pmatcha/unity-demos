﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayShooter : MonoBehaviour
{
    private Camera _camera;

    [SerializeField] private GameObject cursorParticleSystem;

    // Start is called before the first frame update
    void Start()
    {
        _camera = GetComponent<Camera>();

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        //cursorParticleSystem.transform.position = new Vector3 (999,999,999); //ewwwwww
    }

    void OnGUI() {
        int size = 12;
        float posX = _camera.pixelWidth/2 - size/2;
        float posY = _camera.pixelHeight/2 - size;
        GUI.Label(new Rect(posX, posY, size, size), "*");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && Cursor.visible == false)
        {
            Vector3 point = new Vector3(_camera.pixelWidth/2, _camera.
            pixelHeight/2, 0);
            Ray ray = _camera.ScreenPointToRay(point);
            RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    //StartCoroutine(SphereIndicator(hit.point));
                    GameObject hitObject = hit.transform.gameObject;
                    ReactiveTarget target = hitObject.GetComponent<ReactiveTarget>();
                    if (target != null) {
                        target.ReactToHit();
                        Messenger.Broadcast(GameEvent.ENEMY_HIT);
                    } else {
                        StartCoroutine(SphereIndicator(hit.point));
                    }
                }
        }

        if (Input.GetKeyDown("escape"))
        {
            if (Cursor.lockState == CursorLockMode.None)
            {
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
                //cursorParticleSystem.transform.position = new Vector3 (999,999,999); //ewwwwww
            }
            else
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
        }

        if (Input.GetMouseButtonDown(1))
        {
            if (Cursor.lockState != CursorLockMode.None)
            {
                Cursor.lockState = CursorLockMode.Confined;
                Cursor.visible = true;
            }
        }

        if (Cursor.lockState == CursorLockMode.Confined)
        {
            updateCursorParticle();
        }
    }

    private IEnumerator SphereIndicator(Vector3 pos)
    {
        GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        sphere.transform.position = pos;

        yield return new WaitForSeconds(1);

        Destroy(sphere);
    }
    
    private void updateCursorParticle()
    {
        
        Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
        Vector3 pos = ray.GetPoint(1);
        Debug.Log("Position of cursor system " + Input.mousePosition.x / _camera.pixelWidth);
        cursorParticleSystem.transform.position = _camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x/3, Input.mousePosition.y/3, 1f));
    }
}
