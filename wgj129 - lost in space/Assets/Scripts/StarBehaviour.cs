﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarBehaviour : MonoBehaviour
{
    [SerializeField] private float moveSpeed_X;
    [SerializeField] private float moveSpeed_Y;
    [SerializeField] private float moveSpeed_Z;
    private float _movY;
    private float _movX;
    private float _movZ;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        _movX += moveSpeed_X * Time.deltaTime;
        _movY += moveSpeed_Y * Time.deltaTime;
        _movZ += moveSpeed_Z * Time.deltaTime;
        Vector3 addMovement = new Vector3(_movX, _movY, _movZ);
        transform.SetPositionAndRotation(transform.position+addMovement, transform.rotation);
    }
}
