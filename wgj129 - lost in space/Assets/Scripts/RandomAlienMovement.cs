﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAlienMovement : MonoBehaviour
{
    public float jumpForce = 10f;
    public float moveSpeed = 15f;
    public float rotSpeedY = 5f;
    public float returnForce = 10f;

    private int randomAction;
    private const int VIBRATE = 0;
    private const int CIRCLE = 1;
    private const int JUMP = 2;
    private const int HIT = 5;
    private Rigidbody alienRigidbody;

    [SerializeField] private Transform motherShip;

    // Start is called before the first frame update
    void Start()
    {
        randomAction = Random.Range(0,3);
        //Debug.Log("Random action assigned: " + randomAction);
        alienRigidbody = GetComponent <Rigidbody>();

        float currentRotX = transform.eulerAngles.x;
        float newRotY = Random.Range(0,360);
        float currentRotZ = transform.eulerAngles.z;
        
        Quaternion rotation = Quaternion.Euler(currentRotX, newRotY, currentRotZ);
        transform.SetPositionAndRotation(transform.position, rotation);
    }

    // Update is called once per frame
    void Update()
    {

        if (alienRigidbody.constraints == RigidbodyConstraints.None)
        {
            randomAction = HIT;
        }

        switch(randomAction)
        {
            case VIBRATE:
                break;

            case CIRCLE:
                float currentRotX = transform.eulerAngles.x;
                float newRotY = transform.eulerAngles.y + rotSpeedY;
                float currentRotZ = transform.eulerAngles.z;
                
                Quaternion rotation = Quaternion.Euler(currentRotX, newRotY, currentRotZ);
                transform.SetPositionAndRotation(transform.position, rotation);
                
                Vector3 appliedMoveForce = transform.forward * moveSpeed;
                alienRigidbody.AddForce(appliedMoveForce, ForceMode.Acceleration);
                break;

            case JUMP:
                bool hitGround = false;
                RaycastHit hit;
                if (Physics.Raycast(transform.position, Vector3.down, out hit))
                {
                    float check = 0.5f;
                    hitGround = hit.distance <= check;
                }

                if (hitGround)
                {
                    Vector3 appliedJumpForce = Vector3.up * jumpForce;
                    alienRigidbody.AddForce(appliedJumpForce, ForceMode.Acceleration);
                }
                break;
            case HIT:
                Vector3 direction = motherShip.position - transform.position;
                alienRigidbody.AddForce(direction.normalized * returnForce, ForceMode.Acceleration);
                break;

            default:
                break;
        }
    }
}
