﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerUFOVolume : MonoBehaviour
{

    private int _numberOfAliens;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.ToString().Contains("alien"))
        {
            if (other.GetComponent<AlienCount>().isCounted() == false)
            {
                other.GetComponent<AlienCount>().setCounted(true);
                _numberOfAliens++;
                Debug.Log("Alien counted!");
                Debug.Log("Total number of aliens: " + _numberOfAliens);
            }
        }
    }
}
