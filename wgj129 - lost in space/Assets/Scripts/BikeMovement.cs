﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BikeMovement : MonoBehaviour
{

    //engine
    float speed;
    private float accel;
    float acceleration;
    //control
    float brakePower;
    float steer;

    private Rigidbody bikeRigidbody;

    // Start is called before the first frame update
    void Start()
    {
        bikeRigidbody = GetComponent <Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 movement;
        movement = transform.TransformDirection(Vector3.forward) * Mathf.Lerp(0,speed,accel);
        movement.y = bikeRigidbody.velocity.y;
        
        if (Input.GetButton("W")) 
        {
            accel += acceleration/100;
            
            if (accel >= 1)
            {
                accel = 1;
            }
            bikeRigidbody.velocity = movement;
        }
        else 
        {
            accel = 0;
        }
   
        if (Input.GetButton("S"))
        {
            bikeRigidbody.drag = brakePower;
        }
        else 
        {
            bikeRigidbody.drag = 0;
        }   
   
        float spinSpeed;
        spinSpeed = bikeRigidbody.velocity.magnitude;
        if (spinSpeed <= 0.001)
        {
            spinSpeed = 0;
        }
   
        //controls
        //transform.rotation.eulerAngles.z = -Input.GetAxis("X") * 12;
        //if (Input.GetAxis("X")  rigidbody.velocity.magnitude > speed/100)
        //{
        //    transform.Rotate(transform.up  * Input.GetAxis("X") * steer);
        //}
        //}
    }
}
