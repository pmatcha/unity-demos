﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienHit : MonoBehaviour
{

    private CharacterController _charController;
    private ControllerColliderHit _contact;
    public float pushForce = 3.0f;

    // Start is called before the first frame update
    void Start()
    {
        _charController = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnControllerColliderHit(ControllerColliderHit hit) {
        //Debug.Log("Hit something");
        _contact = hit;
        

        if (hit.collider.gameObject.ToString().Contains("alien"))
        {
            ParticleSystem _explosionParticle = hit.collider.GetComponent<ParticleSystem>();
            _explosionParticle.Play();

            AudioSource _explosionAudio = hit.collider.GetComponent<AudioSource>();
            _explosionAudio.Play();
        }
    }
}
