﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienCount : MonoBehaviour
{
    private bool _isCounted;
    // Start is called before the first frame update
    void Start()
    {
        _isCounted = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setCounted(bool counted)
    {
        _isCounted = counted;
    }

    public bool isCounted()
    {
        return _isCounted;
    }
}
