﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class RelativeMovementBike : MonoBehaviour
{
    public float jumpSpeed = 90.0f;
    public float gravity = -9.8f;
    public float terminalVelocity = -10.0f;
    public float minFall = -1.5f;
    public float rotSpeed = 15.0f;
    public float moveSpeed = 6.0f;
    private float _vertSpeed;
    public float pushForce = 3.0f;

    private float _initialSpeed;
    private Vector3 _lastMovement;
    [SerializeField] private AudioSource soundSource;
    [SerializeField] private AudioClip jumpSound;

    [SerializeField] private Transform target;
    private CharacterController _charController;
    private ControllerColliderHit _contact;
    // private Animator _animator;
    // Start is called before the first frame update
    void Start()
    {
        _charController = GetComponent<CharacterController>();
        _vertSpeed = minFall;
        _initialSpeed = moveSpeed;

        // _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

        Vector3 movement = Vector3.zero;
        float horInput = Input.GetAxis("Horizontal");
        float vertInput = Input.GetAxis("Vertical");
        
        bool hitGround = false;
        RaycastHit hit;

        if (horInput != 0 || vertInput != 0)
        {
            //Debug.Log("Moving!");
            movement.x = horInput * moveSpeed;
            movement.z = vertInput * moveSpeed;
            movement = Vector3.ClampMagnitude(movement, moveSpeed);
            //movement = movement + _lastMovement;

            Quaternion tmp = target.rotation;
            target.eulerAngles = new Vector3(0, target.eulerAngles.y, 0);
            movement = target.TransformDirection(movement);
            target.rotation = tmp;
            Quaternion direction = Quaternion.LookRotation(movement);
            transform.rotation = Quaternion.Lerp(transform.rotation, direction, rotSpeed * Time.deltaTime);
            //Debug.Log("Movement:" + movement);
            moveSpeed += 0.1f;
        } else
        {
            moveSpeed = _initialSpeed;
            movement = target.TransformDirection(movement);
        }

        // _animator.SetFloat("Speed", movement.sqrMagnitude);

        if (_vertSpeed < 0 && Physics.Raycast(transform.position, Vector3.down, out hit))
        {
            //Debug.Log("Casting ray to gound");
            float check = 0.5f;
            hitGround = hit.distance <= check;
        }

        if (hitGround)
        {
            //Debug.Log("Ground hit!");
            // _animator.SetBool("Falling", false);
            if (Input.GetButtonDown("Jump"))
            {
                _vertSpeed = jumpSpeed;
                // _animator.SetBool("Jumping", true);
                soundSource.PlayOneShot(jumpSound);
            }
            else
            {
                _vertSpeed = minFall;
                // _animator.SetBool("Jumping", false);
            }
            // _animator.SetFloat("VertSpeed", _vertSpeed);
        }
        else
        {
            //Debug.Log("Falling!");
            _vertSpeed += gravity * 5 * Time.deltaTime;
            if (_vertSpeed < terminalVelocity) {
                _vertSpeed = terminalVelocity;
            }

            if (_contact != null ) {
                // _animator.SetBool("Falling", true);
                //Debug.Log(_vertSpeed.ToString());
            }

            if (_charController.isGrounded)
            {
                // _animator.SetBool("Jumping", false);
                if (Vector3.Dot(movement, _contact.normal) < 0)
                {
                    movement = _contact.normal * moveSpeed;
                }
                else
                {
                    movement += _contact.normal * moveSpeed;
                }
                    
            
            }
            // _animator.SetFloat("VertSpeed", _vertSpeed);
        }
        movement.y = _vertSpeed;
        
        movement *= Time.deltaTime;
        _charController.Move(movement);
    }

    void OnControllerColliderHit(ControllerColliderHit hit) {
        //Debug.Log("Hit something");
        _contact = hit;
        Rigidbody body = hit.collider.attachedRigidbody;
        if (body != null && !body.isKinematic)
        {
            body.constraints = RigidbodyConstraints.None;
            Vector3 newMoveDirection = new Vector3(hit.moveDirection.x, Mathf.Abs(hit.moveDirection.y) + 0.5f, hit.moveDirection.z);
            body.velocity = newMoveDirection * pushForce;
            body.AddRelativeTorque(new Vector3(Random.Range(0f,2f), Random.Range(0f,2f), Random.Range(0f,2f)), ForceMode.Impulse);
        }
    }
}
