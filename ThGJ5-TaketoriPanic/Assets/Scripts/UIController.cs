﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{
    private int _kaguyaHP;
    private int _rabbitNumber;
    private bool _tokiWIN;
    private bool _youLOSE;
    private int _waveNumber;
    
    public int healAmount;
    public int maxKaguyaHp;
    public float _timer = 180f;

    [SerializeField] private Text rabbitNumberLabel;
    [SerializeField] private Text timerLabel;
    [SerializeField] private Slider kaguyaSlider;
    //[SerializeField] private SettingsPopup settingsPopup;

    // Start is called before the first frame update
    void Start()
    {
        _kaguyaHP = maxKaguyaHp;
        _rabbitNumber = 0;
        rabbitNumberLabel.text = _rabbitNumber.ToString();
        kaguyaSlider.value = (maxKaguyaHp/_kaguyaHP);
        //settingsPopup.Close();
    }

    // Update is called once per frame
    void Update()
    {
        _timer -= Time.deltaTime;
        if (_timer <= 0)
        {
            onWin();
        }
        string minutes = Mathf.Floor(_timer / 60).ToString("00");
        string seconds = (_timer % 60).ToString("00");
     
        timerLabel.text = string.Format("{0}:{1}", minutes, seconds);
        //scoreLabel.text = Time.realtimeSinceStartup.ToString();
    }

    public void OnOpenSettings()
    {
        //settingsPopup.Open();
    }

    void Awake()
    {
        Messenger.AddListener(GameEvent.KAGUYA_HIT, onKaguyaHit);
        Messenger.AddListener(GameEvent.KAGUYA_HEAL, onKaguyaHeal);
        Messenger.AddListener(GameEvent.ENEMY_SPAWNED, onEnemySpawned);
        Messenger.AddListener(GameEvent.ENEMY_DEFEATED, onEnemyDefeated);
        Messenger.AddListener(GameEvent.LOSE, onLose);
        Messenger.AddListener(GameEvent.WIN, onWin);
        Messenger.AddListener(GameEvent.WAVE_END, onWaveEnd);
    }
    void OnDestroy()
    {
        Messenger.RemoveListener(GameEvent.KAGUYA_HIT, onKaguyaHit);
        Messenger.RemoveListener(GameEvent.KAGUYA_HEAL, onKaguyaHeal);
        Messenger.RemoveListener(GameEvent.ENEMY_SPAWNED, onEnemySpawned);
        Messenger.RemoveListener(GameEvent.ENEMY_DEFEATED, onEnemyDefeated);
        Messenger.RemoveListener(GameEvent.LOSE, onLose);
        Messenger.RemoveListener(GameEvent.WIN, onWin);
        Messenger.RemoveListener(GameEvent.WAVE_END, onWaveEnd);
    }

/*     private void OnEnemyHit()
    {
        _score += 1;
        scoreLabel.text = _score.ToString();
    } */

    private void onKaguyaHit()
    {
        _kaguyaHP--;
        Debug.Log("Kaguya hp: "+ _kaguyaHP);
        kaguyaSlider.value = ((float)_kaguyaHP/(float)maxKaguyaHp);

        if (_kaguyaHP < 0)
        {
            onLose();
        }
    }
    private void onKaguyaHeal()
    {
        _kaguyaHP += healAmount;
        kaguyaSlider.value = ((float)_kaguyaHP/(float)maxKaguyaHp);
    }
    private void onEnemySpawned()
    {
        _rabbitNumber++;
        rabbitNumberLabel.text = _rabbitNumber.ToString();
    }
    private void onEnemyDefeated()
    {
        _rabbitNumber--;
        rabbitNumberLabel.text = _rabbitNumber.ToString();
    }
    private void onLose()
    {
        StartCoroutine(LoadSceneWait("Lose"));
    }
    private void onWin()
    {
        StartCoroutine(LoadSceneWait("Win"));
    }
    private void onWaveEnd()
    {
        // RESET TIMER
    }

    private IEnumerator LoadSceneWait(string sceneName)
    {

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        Debug.Log("Unlocking cursor");
        SceneManager.LoadScene(sceneName);
        yield return 0; // wait a frame, so it can finish loading
        SceneManager.SetActiveScene( SceneManager.GetSceneByName( sceneName ) );
    }
}
