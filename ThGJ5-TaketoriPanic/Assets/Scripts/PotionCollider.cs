﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotionCollider : MonoBehaviour
{

    private bool _isHeld = false;
    public int _type = 3;
    private GameObject incomingPotion;

    private const int BLUE = 0;
    private const int PURPLE = 1;
    private const int GREEN = 2;
    private const int UNUSED = 3;
    private const int RED = 4;
    private const int GOLD = 5;


    [SerializeField] private GameObject BluePotionEffect;
    [SerializeField] private GameObject PurplePotionEffect;
    [SerializeField] private GameObject GreenPotionEffect;
    [SerializeField] private GameObject RedPotionEffect;
    [SerializeField] private GameObject GoldenPotionEffect;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter(Collision collision)
    {
        if (_isHeld)
        {
            GameObject colliderObject = collision.collider.gameObject;
            string colliderName = colliderObject.ToString();
            if (colliderName.Contains("Rabbit"))
            {
                if (colliderName.Contains("green") && _type == GREEN)
                {
                    Debug.Log("hit green rabbit!");
                    GameObject potionEffect = Instantiate(GreenPotionEffect) as GameObject;
                    potionEffect.transform.position = this.transform.position;
                    Destroy(this.gameObject);
                }
                else if (colliderName.Contains("purple") && _type == PURPLE)
                {
                    Debug.Log("hit purple rabbit!");
                    GameObject potionEffect = Instantiate(PurplePotionEffect) as GameObject;
                    potionEffect.transform.position = this.transform.position;
                    Destroy(this.gameObject);
                }
                else if (colliderName.Contains("blue") && _type == BLUE)
                {
                    Debug.Log("hit blue rabbit!");
                    GameObject potionEffect = Instantiate(BluePotionEffect) as GameObject;
                    potionEffect.transform.position = this.transform.position;
                    Destroy(this.gameObject);
                }
                else if (_type == GOLD)
                {
                    Debug.Log("hit any rabbit!");
                    GameObject potionEffect = Instantiate(GreenPotionEffect) as GameObject;
                    GameObject potionEffect2 = Instantiate(BluePotionEffect) as GameObject;
                    GameObject potionEffect3 = Instantiate(GreenPotionEffect) as GameObject;
                    GameObject potionEffect4 = Instantiate(GoldenPotionEffect) as GameObject;
                    potionEffect.transform.position = this.transform.position;
                    potionEffect2.transform.position = this.transform.position;
                    potionEffect3.transform.position = this.transform.position;
                    potionEffect4.transform.position = this.transform.position;
                    Destroy(this.gameObject);
                }
                else 
                {

                }
            }
            else if (colliderName.Contains("kaguya"))
            {
                if (_type ==RED)
                {
                   Debug.Log("healing kaguya!");
                   GameObject potionEffect = Instantiate(RedPotionEffect) as GameObject;
                   potionEffect.transform.SetParent(colliderObject.transform);
                   potionEffect.transform.localPosition = Vector3.zero;
                   Messenger.Broadcast(GameEvent.KAGUYA_HEAL);
                   AudioSource.PlayClipAtPoint(colliderObject.GetComponent<AudioSource>().clip, transform.position);
                   Destroy(this.gameObject);
                }
            }
            else if (colliderName.Contains("Potion"))
            {
                Destroy(colliderObject);
                this.transform.parent.GetComponentInChildren<PotionCollector>().potionCheck(colliderName);
            }
        }
        
    }

    public void isBeingHeld (bool holding)
    {
        _isHeld = holding;
    }

    public void setType (int type)
    {
        _type = type;
    }
}
