﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerEnemies : MonoBehaviour
{
    public float spawnRate = 5f; //in seconds
    public int spawnRadius = 50; //in meters
    public float spawnHeight = 1f; //in meters

    private int enemyType;
    private Vector3 randomPosition = Vector3.zero;
    private float counter;
    private const int BLUE = 0;
    private const int PURPLE = 1;
    private const int GREEN = 2;
    private const int UNUSED = 3;
    private const int RED = 4;
    private const int GOLD = 5;
    List<GameObject> _enemies = new List<GameObject>();

    [SerializeField] private GameObject blueRabbit;
    [SerializeField] private GameObject purpleRabbit;
    [SerializeField] private GameObject greenRabbit;
    [SerializeField] private GameObject target;
    // Start is called before the first frame update
    void Start()
    {
        counter = 0;

    }

    // Update is called once per frame
    void Update()
    {
        counter += Time.deltaTime;

        if (counter > spawnRate)
        {
            counter = 0;

            randomPosition.x = transform.position.x + Random.Range(-spawnRadius, spawnRadius);
            randomPosition.y = spawnHeight;
            randomPosition.z = transform.position.z + Random.Range(-spawnRadius, spawnRadius);
            enemyType = Random.Range(0,3);
            GameObject newEnemy;

            switch(enemyType)
            {
                case BLUE:
                {
                    newEnemy = Instantiate(blueRabbit) as GameObject;
                    newEnemy.transform.position = randomPosition;
                    _enemies.Add(newEnemy);
                    
                    newEnemy.GetComponent<EnemyAIMovement>().setTarget(target);
                    break;
                }
                
                case PURPLE:
                {
                    newEnemy = Instantiate(purpleRabbit) as GameObject;
                    newEnemy.transform.position = randomPosition;
                    _enemies.Add(newEnemy);
                    
                    newEnemy.GetComponent<EnemyAIMovement>().setTarget(target);
                    break;
                }
                
                case GREEN:
                {
                    newEnemy = Instantiate(greenRabbit) as GameObject;
                    newEnemy.transform.position = randomPosition;
                    _enemies.Add(newEnemy);
                    
                    newEnemy.GetComponent<EnemyAIMovement>().setTarget(target);
                    break;
                }

                default:
                    break;
            }
            
            Messenger.Broadcast(GameEvent.ENEMY_SPAWNED);
        }
    }
}
