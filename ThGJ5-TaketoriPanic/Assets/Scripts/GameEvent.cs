﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameEvent {
public const string KAGUYA_HIT = "KAGUYA_HIT";
public const string KAGUYA_HEAL = "KAGUYA_HEAL";
public const string ENEMY_SPAWNED = "ENEMY_SPAWNED";
public const string ENEMY_DEFEATED = "ENEMY_DEFEATED";
public const string LOSE = "LOSE";
public const string WIN = "WIN";
public const string WAVE_END = "WAVE_END";
}