﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotionCollector : MonoBehaviour
{
    private ControllerColliderHit _contact;
    
    private bool isHoldingPotion = false;
    private GameObject potionHeld;
    private int potionHeldType;
    private int newPotionType;
    private const int BLUE = 0;
    private const int PURPLE = 1;
    private const int GREEN = 2;
    private const int UNUSED = 3;
    private const int RED = 4;
    private const int GOLD = 5;

    [SerializeField] private GameObject bluePotion;
    [SerializeField] private GameObject purplePotion;
    [SerializeField] private GameObject greenPotion;
    [SerializeField] private GameObject redPotion;
    [SerializeField] private GameObject goldenPotion;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (potionHeld != null)
        {
            potionHeld.transform.localPosition = Vector3.zero;
        }
    }

    public void potionCheck (string potionType)
    {
        if (potionHeld != null)
        {
            Destroy(potionHeld);
        }

        Debug.Log("Getting potion");
        if (potionType.Contains("Blue"))
        {
            potionHeldType = BLUE;
            potionHeld = Instantiate(bluePotion) as GameObject;

        }
        else if (potionType.Contains("Red"))
        {
            potionHeldType = RED;
            potionHeld = Instantiate(redPotion) as GameObject;
        }
        else if (potionType.Contains("Purple"))
        {
            potionHeldType = PURPLE;
            potionHeld = Instantiate(purplePotion) as GameObject;

        }
        else if (potionType.Contains("Green"))
        {
            potionHeldType = GREEN;
            potionHeld = Instantiate(greenPotion) as GameObject;

        }
        else if (potionType.Contains("Golden"))
        {
            potionHeldType = GOLD;
            potionHeld = Instantiate(goldenPotion) as GameObject;

        }
        else
        {
            potionHeldType = GOLD;
            potionHeld = Instantiate(goldenPotion) as GameObject;
        }

        potionHeld.GetComponent<PotionCollider>().isBeingHeld(true);
        potionHeld.GetComponent<PotionCollider>().setType(potionHeldType);

        potionHeld.transform.SetParent(this.transform, true);
        potionHeld.transform.localPosition = Vector3.zero;
        potionHeld.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;

    }

    public int getPotionTypeHeld()
    {
        return potionHeldType;
    }
}
