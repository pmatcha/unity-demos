﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerPotions : MonoBehaviour
{
    public float spawnRate = 5f; //in seconds
    public int spawnRadius = 50; //in meters
    public float spawnHeight = 1f; //in meters

    private int potionType;
    private Vector3 randomPosition = Vector3.zero;
    private float counter;
    private const int BLUE = 0;
    private const int PURPLE = 1;
    private const int GREEN = 2;
    private const int UNUSED = 3;
    private const int RED = 4;
    private const int GOLD = 5;
    List<GameObject> _potions = new List<GameObject>();

    [SerializeField] private GameObject bluePotion;
    [SerializeField] private GameObject purplePotion;
    [SerializeField] private GameObject greenPotion;
    [SerializeField] private GameObject redPotion;
    [SerializeField] private GameObject goldenPotion;

    // Start is called before the first frame update
    void Start()
    {
        counter = 0;
    }

    // Update is called once per frame
    void Update()
    {
        counter += Time.deltaTime;

        if (counter > spawnRate)
        {
            counter = 0;

            randomPosition.x = transform.position.x + Random.Range(-spawnRadius, spawnRadius);
            randomPosition.y = spawnHeight;
            randomPosition.z = transform.position.z + Random.Range(-spawnRadius, spawnRadius);
            potionType = Random.Range(0,6);
            GameObject newPotion;

            switch(potionType)
            {
                case BLUE:
                {
                    newPotion = Instantiate(bluePotion) as GameObject;
                    newPotion.transform.position = randomPosition;
                    _potions.Add(newPotion);
                    break;
                }
                
                case PURPLE:
                {
                    newPotion = Instantiate(purplePotion) as GameObject;
                    newPotion.transform.position = randomPosition;
                    _potions.Add(newPotion);
                    break;
                }
                
                case GREEN:
                {
                    newPotion = Instantiate(greenPotion) as GameObject;
                    newPotion.transform.position = randomPosition;
                    _potions.Add(greenPotion);
                    break;
                }
                
                case RED:
                {
                    newPotion = Instantiate(redPotion) as GameObject;
                    newPotion.transform.position = randomPosition;
                    _potions.Add(redPotion);
                    break;
                }

                case GOLD:
                {
                    newPotion = Instantiate(goldenPotion) as GameObject;
                    newPotion.transform.position = randomPosition;
                    _potions.Add(goldenPotion);
                    break;
                }

                default:
                    break;
            }
        }
    }
}
