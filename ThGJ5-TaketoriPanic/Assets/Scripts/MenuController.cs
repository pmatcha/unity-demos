﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnClickStart()
    {
        StartCoroutine(LoadSceneWait("Instructions"));
    }

    public void OnClickInstructions()
    {
        StartCoroutine(LoadSceneWait("SampleScene"));
    }

    public void OnClickCredits()
    {
        SceneManager.LoadScene("Credits");
    }

    public void OnClickLose()
    {
        SceneManager.LoadScene("MainMenu");
    }


    public void OnClickExit()
    {
        Application.Quit();
    }

    private IEnumerator LoadSceneWait(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
        yield return 0; // wait a frame, so it can finish loading
        SceneManager.SetActiveScene( SceneManager.GetSceneByName( sceneName ) );
    }
}
