﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KaguyaController : MonoBehaviour
{
    public int maxHp = 10000;

    private int _hp = 9999;

    // Start is called before the first frame update
    void Start()
    {
        _hp = maxHp;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void substractHp(int hit)
    {
        _hp -= hit;
        Messenger.Broadcast(GameEvent.KAGUYA_HIT);
    }

    public void addHp(int heal)
    {
        _hp += heal;
        Messenger.Broadcast(GameEvent.KAGUYA_HEAL);
    }
}
