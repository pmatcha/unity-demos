﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RabbitCollider : MonoBehaviour
{

    private Rigidbody body;
    
    [SerializeField] private GameObject explosion;
    public int _type = 3;

    private const int BLUE = 0;
    private const int PURPLE = 1;
    private const int GREEN = 2;
    private const int UNUSED = 3;
    private const int RED = 4;
    private const int GOLD = 5;
    // Start is called before the first frame update
    void Start()
    {
        body = this.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider collision)
    {
        GameObject colliderObject = collision.gameObject;
        string colliderName = colliderObject.ToString();
        
        Debug.Log(colliderName);
        if (colliderName.Contains("Effect"))
        {
            if ((colliderName.Contains("Green") && _type == GREEN)
            || (colliderName.Contains("Purple") && _type == PURPLE)
            || (colliderName.Contains("Blue") && _type == BLUE)
            || (colliderName.Contains("Gold")))
            {
                Debug.Log("rabbit dies!");
                GameObject explosionEffect = Instantiate(explosion) as GameObject;
                explosionEffect.transform.position = this.transform.position;

                body.constraints = RigidbodyConstraints.None;
                Vector3 newMoveDirection = new Vector3(Random.Range(0f, 1f), Mathf.Abs(Random.Range(0f, 1f)) + 2f, Random.Range(0f, 1f));
                body.AddForce(newMoveDirection*50f, ForceMode.Impulse);
                //body.AddExplosionForce(50f, this.transform.position,1f, 1f);
                body.AddRelativeTorque(new Vector3(Random.Range(0f,2f), Random.Range(0f,2f),   Random.Range(0f,2f)), ForceMode.Impulse);
                
                Messenger.Broadcast(GameEvent.ENEMY_DEFEATED);
                this.GetComponent<SelfDestroy>().startSelfDestruction(true);
                this.GetComponent<EnemyAIMovement>().stop();
                AudioSource.PlayClipAtPoint(this.GetComponent<AudioSource>().clip, transform.position);
            }
        }
            

    }

    void OnCollisionEnter(Collision collision)
    {
        GameObject colliderObject = collision.collider.gameObject;
        string colliderName = colliderObject.ToString();
        if (colliderName.Contains("kaguya"))
        {
            colliderObject.GetComponent<KaguyaController>().substractHp(1);
            Debug.Log("rabbit dies!");
            GameObject explosionEffect = Instantiate(explosion) as GameObject;
            explosionEffect.transform.position = this.transform.position;
            Messenger.Broadcast(GameEvent.ENEMY_DEFEATED);
            AudioSource.PlayClipAtPoint(this.GetComponent<AudioSource>().clip, transform.position);
            Destroy(gameObject);
        }
    }
}
