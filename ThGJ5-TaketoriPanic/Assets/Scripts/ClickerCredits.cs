﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickerCredits : MonoBehaviour
{

    [SerializeField] private GameObject buttonImage;
    [SerializeField] private GameObject twButton;
    [SerializeField] private GameObject bcButton;
    [SerializeField] private Sprite creditsSprite;

    private int counter;
    // Start is called before the first frame update
    void Start()
    {
        counter = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void onClickAdvance()
    {
        switch(counter)
        {
            case 0:
            buttonImage.GetComponent<Image>().sprite = creditsSprite;
            twButton.GetComponent<RectTransform>().sizeDelta = new Vector2(100f,100f);
            bcButton.GetComponent<RectTransform>().sizeDelta = new Vector2(112f,100f);
            break;
            case 1:
            this.GetComponent<MenuController>().OnClickLose();
            break;
            case 2:
            break;
        }
        counter++;
    }

    public void onClickTwitter()
    {
        Application.OpenURL("https://twitter.com/asfdsgsdhhshdfh");
    }
    public void onClickBandcamp()
    {
        Application.OpenURL("https://nephetheus.bandcamp.com/");
    }
}
