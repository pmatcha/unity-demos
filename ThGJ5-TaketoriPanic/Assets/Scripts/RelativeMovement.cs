﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class RelativeMovement : MonoBehaviour
{
    public float jumpSpeed = 90.0f;
    public float gravity = -9.8f;
    public float terminalVelocity = -10.0f;
    public float minFall = -1.5f;
    public float rotSpeed = 15.0f;
    public float moveSpeed = 6.0f;
    private float _vertSpeed;
    public float pushForce = 3.0f;
    public float distanceToGround = 1.5f;

    [SerializeField] private AudioSource soundSource;
    [SerializeField] private AudioClip jumpSound;

    [SerializeField] private Transform target;
    private CharacterController _charController;
    private ControllerColliderHit _contact;
    //private Animator _animator;
    // Start is called before the first frame update
    void Start()
    {
        _charController = GetComponent<CharacterController>();
        _vertSpeed = minFall;
        //_animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 movement = Vector3.zero;

        float horInput = Input.GetAxis("Horizontal");
        float vertInput = Input.GetAxis("Vertical");
        
        bool hitGround = false;
        RaycastHit hit;
        
        if (horInput != 0 || vertInput != 0)
        {
            movement.x = horInput * moveSpeed;
            movement.z = vertInput * moveSpeed;
            movement = Vector3.ClampMagnitude(movement, moveSpeed);

            Quaternion tmp = target.rotation;
            target.eulerAngles = new Vector3(0, target.eulerAngles.y, 0);
            movement = target.TransformDirection(movement);
            target.rotation = tmp;
            Quaternion direction = Quaternion.LookRotation(movement);
            transform.rotation = Quaternion.Lerp(transform.rotation, direction, rotSpeed * Time.deltaTime);
        }
        //_animator.SetFloat("Speed", movement.sqrMagnitude);

        if (_vertSpeed < 0 && Physics.Raycast(transform.position, Vector3.down, out hit))
        {
            float check = distanceToGround;
            hitGround = hit.distance <= check;
        }

        if (hitGround)
        {
            //_animator.SetBool("Falling", false);
            if (Input.GetButtonDown("Jump"))
            {
                _vertSpeed = jumpSpeed;
                //_animator.SetBool("Jumping", true);
                            AudioSource.PlayClipAtPoint(this.GetComponent<AudioSource>().clip, transform.position);

            }
            else
            {
                _vertSpeed = minFall;
                //_animator.SetBool("Jumping", false);
            }
            //_animator.SetFloat("VertSpeed", _vertSpeed);
        }
        else
        {
            _vertSpeed += gravity * 5 * Time.deltaTime;
            if (_vertSpeed < terminalVelocity) {
                _vertSpeed = terminalVelocity;
            }

            if (_contact != null ) {
                //_animator.SetBool("Falling", true);
            }

            if (_charController.isGrounded)
            {
                //_animator.SetBool("Jumping", false);
                if (Vector3.Dot(movement, _contact.normal) < 0)
                {
                    movement = _contact.normal * moveSpeed;
                }
                else
                {
                    movement += _contact.normal * moveSpeed;
                }
                    
            
            }
            //_animator.SetFloat("VertSpeed", _vertSpeed);
        }
        movement.y = _vertSpeed;
        
        movement *= Time.deltaTime;
        _charController.Move(movement);
    }

    void OnControllerColliderHit(ControllerColliderHit hit) {
        _contact = hit;
        Rigidbody body = hit.collider.attachedRigidbody;
        if (body != null && !body.isKinematic)
        {
            body.velocity = hit.moveDirection * pushForce;
        }

        if (hit.collider.gameObject.ToString().Contains("Potion"))
        {
            Debug.Log("Collected potion!");
            string potionName = hit.collider.gameObject.ToString();
            AudioSource.PlayClipAtPoint(hit.collider.gameObject.GetComponent<AudioSource>().clip, transform.position);
            Destroy(hit.collider.gameObject);
            this.GetComponentInChildren<PotionCollector>().potionCheck(potionName);
                
        }
    }
    
}