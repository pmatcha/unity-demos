﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAIMovement : MonoBehaviour
{

    [SerializeField] private GameObject target;

    private bool _isDead = false;

    public float moveSpeed = 10f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (_isDead == false)
        {
            float step = moveSpeed * Time.deltaTime;
            
            Vector3 oldPosition = transform.position;

            transform.position = Vector3.MoveTowards(transform.position, target.transform.position, step);

            Vector3 newPosition = transform.position;
            Vector3 directionOfMovement = newPosition - oldPosition;
            directionOfMovement.y = 0;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(directionOfMovement), 0.15f);
        }

    }

    public void setTarget(GameObject incomingTarget)
    {
        target = incomingTarget;
    }

    public void stop()
    {
        _isDead = true;
    }
}
