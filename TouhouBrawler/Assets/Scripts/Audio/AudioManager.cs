using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    [Header("Sounds")]
    public Sound[] sounds;
    public AudioMixerGroup sound;
    public AudioMixerGroup music;

    [Header("Volume")]
    [Range(0f, 1f)] public float musicVolume = 1f;
    [Range(0f, 1f)] public float soundVolume = 1f;
    public AudioClip currentMusic;

    public static AudioManager Instance;

    private void Awake()
    {
        Instance = this;

        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            if (s.isMusic)
            {
                s.source.loop = true;
                s.source.outputAudioMixerGroup = music;
            }
            else
                s.source.outputAudioMixerGroup = sound;
        }
    }

    // Update is called once per frame
    void Update()
    {
        Volume();
    }

    void Volume()
    {
        foreach (Sound s in sounds)
        {
            if (s.isMusic)
                s.source.volume = musicVolume;
            else
                s.source.volume = soundVolume;

        }
    }

    public void Play(string soundName)
    {
        if (GetAudioByClipName(soundName) == null)
        {
            Debug.Log(GetAudioByClipName(soundName) + " audio not found.");
            return;
        }

        GetAudioByClipName(soundName).source.Play();
    }

    public void Stop(string soundName)
    {
        if (GetAudioByClipName(soundName) == null)
        {
            Debug.Log(GetAudioByClipName(soundName) + " audio not found.");
            return;
        }

        GetAudioByClipName(soundName).source.Stop();
    }

    Sound GetAudioByClipName(string n)
    {
        Sound r = null;

        for (int i = 0; i < sounds.Length; i++)
        {
            if (sounds[i].soundName == n)
            {
                r = sounds[i];
            }
        }

        return r;
    }
}

[System.Serializable]
public class Sound
{
    public string soundName;
    public AudioClip clip;
    public bool isMusic;

    [HideInInspector]
    public AudioSource source;
}
