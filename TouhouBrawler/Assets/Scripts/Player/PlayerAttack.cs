using CMF;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    public bool isAttacking;
    public bool isSnapping;
    public bool canAttack;
    public bool attackUp;
    public int currentDamageOutput;
    public int damageOuput = 1;
    public int attackUpDamagePoints = 2;
    public string[] attackStrings;
    public float comboResetTimer = 0.2f;
    public Transform mouseTarget;
    public float snapRequiredDistance = 3f;
    public float snapStopDistance = 0.5f;
    public float snapAngle = 140f;
    public float snapMoveSpeed = 50f;
    public float attackUpTime = 15f;
    public Vector3 snapOffset;
    public PlayerDamageCollider damageCollider;
    Vector3 mousePosition;
    SmoothPosition smoothPosition;
    TurnTowardControllerVelocity turnToward;
    private bool _beganComboReset;
    private bool _comboIncreased;
    private int _comboIndex;
    private float _comboResetTimer;
    private float _attackUpTimer;
    Animator animator;
    private Enemy currentEnemy;

    public float isAttackingTimer = 5f;
    private float _isAttackingTimer;

    public static PlayerAttack Instance;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        smoothPosition = GetComponentInChildren<SmoothPosition>();
        turnToward = GetComponentInChildren<TurnTowardControllerVelocity>();
        animator = GetComponentInChildren<Animator>();

        //damageCollider = GetComponentInChildren<PlayerDamageCollider>();
        damageCollider.GetInit(this);

        currentDamageOutput += damageOuput;

        currentEnemy = null;
        canAttack = true;
    }

    // Update is called once per frame
    void Update()
    {
        MousePosition();
        AttackInput();
        ComboReset();
        IsAttackSnapping();
        AttackUpLength();

        if(isAttacking)
        {
            if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Z))
                _isAttackingTimer = 0;

            if (_isAttackingTimer < attackUpTime)
                _isAttackingTimer += Time.deltaTime;
            else
            {
                _isAttackingTimer = 0f;
                isAttacking = false;
            }
        }
    }

    void AttackUpLength()
    {
        if(attackUp)
        {
            if (_attackUpTimer < attackUpTime)
                _attackUpTimer += Time.deltaTime;
            else
            {
                AttackUpStop();
            }
        }
    }

    void MousePosition()
    {
        Plane plane = new Plane(Vector3.up, 0f);
        Ray ray = GameObject.Find("Player Camera").GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
        float distanceToPlane;

        if (plane.Raycast(ray, out distanceToPlane))
        {
            mousePosition = ray.GetPoint(distanceToPlane);
        }
    }

    public void AttackUpStart()
    {
        currentDamageOutput += attackUpDamagePoints;
        attackUp = true;
    }

    void AttackUpStop()
    {
        currentDamageOutput -= attackUpDamagePoints;
        _attackUpTimer = 0f;
        attackUp = false;
    }
    
    void AttackInput()
    {
        if ((Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Z)) && canAttack && !DialogueManager.Instance.textObj.activeInHierarchy)
        {
            currentEnemy = ClosestEnemy();

            if(currentEnemy != null)
            {
                StartAttack(currentEnemy.transform);
            }
            else
                StartAttack(null);
        }
    }

    Enemy ClosestEnemy()
    {
        Enemy e = null;

        for(int i = 0; i < EnemiesInScene.Instance.enemies.Count; i++)
        {
            if(Vector3.Distance(EnemiesInScene.Instance.enemies[i].transform.position, this.transform.position) < snapRequiredDistance)
            {
                e = EnemiesInScene.Instance.enemies[i];

                Vector3 forward = this.transform.TransformDirection(Vector3.forward);
                Vector3 toOther = e.transform.position - this.transform.position;

                if (Vector3.Dot(forward, toOther) < snapAngle)
                {
                    e = EnemiesInScene.Instance.enemies[i];
                    return e;
                }
                else
                {
                    return null;
                }
            }
        }

        return null;
    }

    void OnMouseClick()
    {
        if(Input.GetMouseButtonDown(0))
        {

            RaycastHit hit;
            Ray ray = GameObject.Find("Player Camera").GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                if(hit.transform != null)
                {
                    Enemy enemy = hit.transform.GetComponent<Enemy>();

                    if(enemy != null)
                    {
                        Debug.Log("Damage");
                       // StartAttack(enemy.transform);
                    }
                    else
                    {
                        //StartAttack(mouseTarget);
                    }
                }
            }
        }
    }

    void ComboReset()
    {
        if (_beganComboReset)
        {
            if (_comboIndex >= attackStrings.Length)
            {
                _comboIndex = 0;
                //_player.ResetVelocity();
                ComboIndexReset();
                //_beganAttackCooldown = true;
                _beganComboReset = false;
            }
            else
            {
                if (_comboResetTimer <= comboResetTimer)
                    _comboResetTimer += Time.deltaTime;
                else
                {
                    _comboResetTimer = 0;
                    //_player.ResetVelocity();
                    ComboIndexReset();
                    _beganComboReset = false;
                }
            }
        }
    }

    void IsAttackSnapping()
    {
        if(isSnapping && currentEnemy != null)
        {
            Debug.Log("Get");
            Vector3 snapTargetPosition = currentEnemy.transform.position - snapOffset;

            float distance = Vector3.Distance(snapTargetPosition, this.transform.position);

            if(distance < snapStopDistance)
            {
                Debug.Log("Snap in Progress");
                this.transform.position = Vector3.MoveTowards(this.transform.position, snapTargetPosition, snapMoveSpeed);
            }
            else
            {
                Debug.Log("Snap Done");
                isSnapping = false;
            }
        }
    }

    void StartAttack(Transform t)
    {
        DamageColliderStatus(false);
        isAttacking = true;
        canAttack = false;

        _comboIncreased = false;
        _beganComboReset = false;

        if (t != null)
        {
            smoothPosition.transform.LookAt(t.position, Vector3.up);
            isSnapping = true;
            animator.Play(attackStrings[_comboIndex]);
        }
        else
            animator.Play(attackStrings[_comboIndex]);
        /* this.transform.position = new Vector3(t.transform.position.x,
             this.transform.position.y, t.position.z);
        */

    }

    public void ContinueAttack()
    {
        ComboIndexIncrease();
        canAttack = true;
        //Can Attack = True;
    }

    public void CloseAttack()
    {
        DamageColliderStatus(false);
        _beganComboReset = true;
        isAttacking = false;
        canAttack = true;
        currentEnemy = null;
    }

    public void AnimationCancel()
    {
        isAttacking = false;
        ComboIndexReset();
    }

    public void DamageColliderStatus(bool on)
    {
        damageCollider.gameObject.SetActive(on);
    }

    public void ComboIndexReset()
    {
        _comboIndex = 0;
    }

    public void ComboIndexIncrease()
    {
        if (!_comboIncreased)
        {
            _comboIndex++;
            _comboIncreased = true;
        }
    }
}
