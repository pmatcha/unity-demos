using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDamageCollider : MonoBehaviour
{
    PlayerAttack attack;
    private new Collider damageCollider;
    bool ready = false;

    public void GetInit(PlayerAttack a)
    {
        this.attack = a;

        damageCollider = GetComponent<Collider>();
        damageCollider.isTrigger = true;

        this.gameObject.layer = LayerMask.NameToLayer("Damage Collider");
        ready = true;
        this.gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.GetComponent<Enemy>() && other.gameObject.layer == LayerMask.NameToLayer("Enemy")
            && ready)
        {
            Debug.Log("Hit");
            other.gameObject.GetComponent<Enemy>().TakeDamage(attack.currentDamageOutput);
            Debug.Log("Hit Done");
        }
    }
}
