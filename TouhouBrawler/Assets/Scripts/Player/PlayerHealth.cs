using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public int health;
    public int healthMax = 10;
    public float iFramesTime = 1f;
    public float invincibilityTime = 7f;
    public bool takingDamage;
    public bool iFrames;
    public bool invincibility;
    public bool isDead;
    public float knockbackSpeed = 5f;
    public MeshRenderer modelRenderer;
    private new Collider damageCollider;
    private float _iframesTime;
    private float _invincibility;
    private SkinnedMeshRenderer model;
    PlayerAttack attack;
    Animator animator;
    private Rigidbody rigid;

    public static PlayerHealth Instance;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        damageCollider = GetComponent<Collider>();
        model = GetComponentInChildren<SkinnedMeshRenderer>();
        attack = GetComponent<PlayerAttack>();
        animator = GetComponentInChildren<Animator>();
        rigid = GetComponent<Rigidbody>();

        health = healthMax;

        this.gameObject.layer = LayerMask.NameToLayer("Player");
    }

    private void Update()
    {
        iFramesUpdate();
        InvincibilityLength();
    }

    public void TakeDamage()
    {
        //Play damage animation
        //Play sound effect
        //Play partciles

        animator.Play("Damage");

        attack.DamageColliderStatus(false);
        attack.isAttacking = false;
        takingDamage = true;
        attack.canAttack = false;
        iFrames = true;

        Vector3 targetVelocity = Vector3.zero;
        targetVelocity = -transform.forward * knockbackSpeed;
        rigid.velocity += targetVelocity;
    }

    public void CloseTakeDamage()
    {
        takingDamage = false;
    }

    void iFramesUpdate()
    {
        if(iFrames)
        {
            if (_iframesTime < iFramesTime)
                _iframesTime += Time.deltaTime;
            else
            {
                _iframesTime = 0;
                //StopCoroutine(Blink(_iframesTime));

                modelRenderer.enabled = true;

                attack.canAttack = true;
                iFrames = false;
            }
        }
    }

    void InvincibilityLength()
    {
        if (invincibility)
        {
            if (_invincibility < invincibilityTime)
                _invincibility += Time.deltaTime;
            else
            {
                StopInvincibility();
            }
        }
    }

    public void StartInvincibility()
    {
        invincibility = true;
    }

    public void StopInvincibility()
    {
        _invincibility = 0f;
        invincibility = false;
    }

    IEnumerator Blink(float waitTime)
    {
        var endTime = Time.time + waitTime;
        while (Time.time < waitTime)
        {
            modelRenderer.enabled = false;
            yield return new WaitForSeconds(0.2f);
            modelRenderer.enabled = true;
            yield return new WaitForSeconds(0.2f);
        }
    }

    public void AddHealth(int health)
    {
        if (this.health < this.healthMax)
            this.health += health;
        else
            this.health = healthMax;
    }

    public void Damage(int health)
    {
        if (!iFrames && !invincibility)
        {
            PlayerHealthBar.Instance.UpdateHealth_UI();

            if (this.health > 1)
            {
                this.health -= health;
                TakeDamage();
            }
            else
            {
                this.health = 0;
                IsDead();
            }
        }
    }

    void IsDead()
    {
        animator.Play("Death");

        isDead = true;
        attack.canAttack = false;

        GameOverScreen.Instance.OpenGameOver();
    }
}
