using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationEvents : MonoBehaviour
{

    PlayerAttack attack;
    PlayerHealth health;

    // Start is called before the first frame update
    void Start()
    {
        attack = GetComponentInParent<PlayerAttack>();
        health = GetComponentInParent<PlayerHealth>();
    }

    public void AnimationEvent_ComboIncrease()
    {
        attack.ComboIndexIncrease();
    }

    public void AnimationEvent_ComboReset()
    {
        attack.ComboIndexReset();
    }

    public void AnimationEvent_EnableDamageCollider()
    {
        attack.DamageColliderStatus(true);
    }

    public void AnimationEvent_DisableDamageCollider()
    {
        attack.DamageColliderStatus(false);
    }

    public void AnimationEvent_CloseAttack()
    {
        attack.CloseAttack();
    }

    public void AnimationEvent_ContinueAttack()
    {
        attack.ContinueAttack();
    }

    public void AnimationEvent_OpenTakingDamage()
    {
        health.TakeDamage();
    }

    public void AnimationEvent_CloseTakingDamage()
    {
        health.CloseTakeDamage();
    }

    public void CloseAttackSnap()
    {
        if (attack.isSnapping)
            attack.isSnapping = false;
    }
}
