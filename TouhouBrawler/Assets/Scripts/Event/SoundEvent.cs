using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEvent : MonoBehaviour
{
    public void PlaySound(string s)
    {
        AudioManager.Instance.Play(s);
    }
}
