using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAngleChange : MonoBehaviour
{
    public Vector3 previousAngle;
    public Vector3 desiredAngle;

    public GameObject cameraRoot;

    private new Collider damageCollider;

    // Start is called before the first frame update
    void Start()
    {
        previousAngle = cameraRoot.transform.localEulerAngles;

        damageCollider = GetComponent<Collider>();
        damageCollider.isTrigger = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.GetComponent<PlayerHealth>())
            cameraRoot.transform.localEulerAngles = desiredAngle;

    }

}
