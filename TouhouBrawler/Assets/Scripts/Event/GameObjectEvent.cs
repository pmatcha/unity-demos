using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameObjectEvent : MonoBehaviour
{
    private new Collider damageCollider;
    public UnityEvent theEvent;

    // Start is called before the first frame update
    void Start()
    {
        damageCollider = GetComponent<Collider>();
        damageCollider.isTrigger = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.GetComponent<PlayerHealth>())
        {
            theEvent.Invoke();
        }
    }
}
