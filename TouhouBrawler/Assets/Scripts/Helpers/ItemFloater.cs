﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemFloater : MonoBehaviour
{
    [SerializeField] private float leeway_X = 0;
    [SerializeField] private float leeway_Y = 0;
    [SerializeField] private float leeway_Z = 0;
    [SerializeField] private float speed_X = 0;
    [SerializeField] private float speed_Y = 0;
    [SerializeField] private float speed_Z = 0;
    private float _accLeewayY = 0;
    private float _accLeewayX = 0;
    private float _accLeewayZ = 0;
    private float _movY = 0;
    private float _movX = 0;
    private float _movZ = 0;
    private bool _ascendingX = true;
    private bool _ascendingY = true;
    private bool _ascendingZ = true;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        //X
        ///////////////////////////
        if (_accLeewayX >= leeway_X)
        {
            _ascendingX = false;
        }
        else if (leeway_X <= -_accLeewayX)
        {
            _ascendingX = true;
        }

        if (_ascendingX)
        {
            _movX = transform.position.x + (speed_X * Time.deltaTime);
            _accLeewayX += speed_X * Time.deltaTime;
        }
        else
        {
            _movX = transform.position.x - (speed_X * Time.deltaTime);
            _accLeewayX += -(speed_X * Time.deltaTime);
        }
        
        //Y
        //////////////////////////
        if (_accLeewayY >= leeway_Y)
        {
            _ascendingY = false;
        }
        else if (leeway_Y <= -_accLeewayY)
        {
            _ascendingY = true;
        }

        if (_ascendingY)
        {
            _movY = transform.position.y + (speed_Y * Time.deltaTime);
            _accLeewayY += speed_Y * Time.deltaTime;
        }
        else
        {
            _movY = transform.position.y - (speed_Y * Time.deltaTime);
            _accLeewayY += -(speed_Y * Time.deltaTime);
        }
        
        //Z
        ///////////////////////
        if (_accLeewayZ >= leeway_Z)
        {
            _ascendingZ = false;
        }
        else if (leeway_Z <= -_accLeewayZ)
        {
            _ascendingZ = true;
        }

        if (_ascendingZ)
        {
            _movZ = transform.position.z + (speed_Z * Time.deltaTime);
            _accLeewayZ += speed_Z * Time.deltaTime;
        }
        else
        {
            _movZ = transform.position.z - (speed_Z * Time.deltaTime);
            _accLeewayZ += -(speed_Z * Time.deltaTime);
        }
        
        
        transform.position = new Vector3(_movX, _movY, _movZ);
    }
}
