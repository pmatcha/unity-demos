﻿using UnityEngine;

[CreateAssetMenu(fileName = "ActionQuitGame", menuName = "Actions/Quit Game", order = 2)]
public class ActionQuitGame : ScriptableObject
{
    public void QuitGame()
    {
    #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
    #else
        Application.Quit();
    #endif
    }
}