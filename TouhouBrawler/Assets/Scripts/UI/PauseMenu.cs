using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public GameObject pauseMenu;
    public bool isPaused;

    public static PauseMenu Instance;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        pauseMenu.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        IsPaused();

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            isPaused = !isPaused;
        }
    }

    public void IsPaused()
    {
        if (isPaused)
        {
            pauseMenu.gameObject.SetActive(true);
            Time.timeScale = 0;
        }
        else
        {
            pauseMenu.gameObject.SetActive(false);
            Time.timeScale = 1f;
        }
    }

    public void ResumeGame()
    {
        isPaused = false;
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("TitleScreen");
        AudioManager.Instance.Stop("Boss");
        AudioManager.Instance.Stop("Level");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
