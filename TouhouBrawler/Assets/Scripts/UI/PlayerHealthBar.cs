using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthBar : MonoBehaviour
{
    public Slider healthBar;
    public Image invImage;
    public Image attackUpImage;

    public static PlayerHealthBar Instance;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        healthBar = GetComponentInChildren<Slider>();

        healthBar.maxValue = PlayerHealth.Instance.healthMax;
        healthBar.value = healthBar.maxValue;
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerHealth.Instance.invincibility)
            invImage.gameObject.SetActive(true);
        else
            invImage.gameObject.SetActive(false);

        if (PlayerAttack.Instance.attackUp)
            attackUpImage.gameObject.SetActive(true);
        else
            attackUpImage.gameObject.SetActive(false);

    }

    public void UpdateHealth_UI()
    {
        healthBar.value = PlayerHealth.Instance.health;
    }
}
