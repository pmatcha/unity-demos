using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TutorialPopup : MonoBehaviour
{
    public TextMeshProUGUI popup;
    public GameObject popupObject;
    public float appearTime = 7f;

    private float _appearTimer;

    public static TutorialPopup Instance;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        popupObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (this.gameObject.activeInHierarchy)
        {
            if (_appearTimer < appearTime)
                _appearTimer += Time.deltaTime;
            else
            {
                ClearPopUp();
            }
        }
    }

    public void ClearPopUp()
    {
        _appearTimer = 0f;
        popup.text = "";
        popupObject.SetActive(false);
    }

    public void OpenPopup(string tut)
    {
        if (popupObject.gameObject.activeInHierarchy)
        {
            _appearTimer = 0f;
            popup.text = tut;
        }
        else
        {
            popup.text = tut;
            popupObject.SetActive(true);
        }
    }
    
}
