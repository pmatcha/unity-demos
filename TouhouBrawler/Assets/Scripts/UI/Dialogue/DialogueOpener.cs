using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueOpener : MonoBehaviour
{
    public bool firstDialogue;
    public bool auto;
    public bool disable;

    private new Collider _collider;

    // Start is called before the first frame update
    void Start()
    {
        _collider = GetComponent<Collider>();
        _collider.isTrigger = true;

        if (auto)
            DialogueManager.Instance.ApplyDialogue(firstDialogue);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.transform.GetComponent<PlayerHealth>() && !auto)
        {
            DialogueManager.Instance.ApplyDialogue(firstDialogue);
            this.gameObject.SetActive(disable);
        }
    }
}
