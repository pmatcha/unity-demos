using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{

    public TextMeshProUGUI dialogueText;
    public GameObject textObj;
    public Image protait;
    public bool dialogueActive;
    bool updateDialog;
    public int index;

    public bool firstScene;

    [System.Serializable]
    public class Dialogue
    {
        [SerializeField] public Sprite character;
        [SerializeField] [TextArea(3, 5)] public string englishText;
        [SerializeField] [TextArea(3, 5)] public string japaneseText;

        public bool lastInIndex;
        public string[] playSounds;
    }

    public Dialogue[] dialogue;
    public Dialogue[] firstDialogue;
    public Dialogue[] secondDialogue;


    public static DialogueManager Instance;
    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        textObj.gameObject.SetActive(false);
      //  ApplyDialogue(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (!dialogueActive)
            return;

        if (!updateDialog)
        {
            updateDialog = true;

            if(MySceneManager.Instance.inEnglish)
                dialogueText.text = dialogue[index].englishText;
            else
                dialogueText.text = dialogue[index].japaneseText;

            SetAudio();

            if (dialogue[index].character != null)
            {
                protait.sprite = dialogue[index].character;
                protait.gameObject.SetActive(true);
            }
            else
            {
                protait.gameObject.SetActive(false);
            }
        }

        if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(KeyCode.Space))
        {
            if (dialogue[index].lastInIndex)
            {
                CloseDialogue();
            }
            else
            {
                updateDialog = false;
                StopAudio();
                index++;
            }
        }


    }

    void SetAudio()
    {
        foreach(string s in dialogue[index].playSounds)
        {
            AudioManager.Instance.Play(s);
        }
    }

    void StopAudio()
    {
        foreach (string s in dialogue[index].playSounds)
        {
            AudioManager.Instance.Stop(s);
        }
    }

    public void ApplyDialogue(bool first)
    {
        TutorialChecker.Instance.DialoguePopup();

        firstScene = first;

        if (firstScene)
            dialogue = firstDialogue;
        else
            dialogue = secondDialogue;

        dialogueActive = true;
        textObj.SetActive(true);
        updateDialog = false;
        index = 0;

        if(!firstScene)
            AudioManager.Instance.Stop("Level");
    }

    public void CloseDialogue()
    {
        index = 0;
        dialogueActive = false;
        textObj.SetActive(false);

        TutorialChecker.Instance.MovePowerPopup();

        if (firstScene)
        {
            AudioManager.Instance.Play("Level");
            firstScene = false;
        }
        else
        {
            AudioManager.Instance.Play("Boss");
            BossHealthBar.Instance.StartBoss();
        }
    }
}