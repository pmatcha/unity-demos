using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverScreen : MonoBehaviour
{
    public GameObject gameOverScreen;
    public bool isPaused;

    public static GameOverScreen Instance;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        gameOverScreen.gameObject.SetActive(false);
    }

    public void OpenGameOver()
    {
        gameOverScreen.gameObject.SetActive(true);
    }

    public void Restart()
    {
        SceneManager.LoadScene("Level");
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("TitleScreen");
        AudioManager.Instance.Stop("Boss");
        AudioManager.Instance.Stop("Level");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
