using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialChecker : MonoBehaviour
{
    public bool attackpowerupPopup;
    public bool invPopup;
    public bool attackpopup;
    public bool movePopup;
    public bool healthPickup;
    public bool dialogueTutorial;

    public static TutorialChecker Instance;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void HealthPickup()
    {
        if (!healthPickup)
        {
            TutorialPopup.Instance.OpenPopup("Your health has been restored!");
            healthPickup = true;
        }
    }

    public void AttackPowerup()
    {
        if (!attackpowerupPopup)
        {
            TutorialPopup.Instance.OpenPopup("You picked up an Attack Powerup! Your attack will go up for a short time!");
            attackpowerupPopup = true;
        }
    }

    public void InvPowerup()
    {
        if (!invPopup)
        {
            TutorialPopup.Instance.OpenPopup("You picked up an Invincibility Powerup! You won't take damage for a short time!");
            invPopup = true;
        }
    }

    public void MovePowerPopup()
    {
        if (!movePopup)
        {
            TutorialPopup.Instance.OpenPopup("Press WASD/Arrow Keys to Move!");
            movePopup = true;
        }
    }

    public void AttackPopup()
    {
        if (!attackpopup)
        {
            TutorialPopup.Instance.OpenPopup("Press Left Mouse Check or Z to Attack!");
            attackpopup = true;
        }
    }

    public void DialoguePopup()
    {
        if (!dialogueTutorial)
        {
            TutorialPopup.Instance.OpenPopup("Press Left Mouse Check, Z or Space to Proceed!");
            dialogueTutorial = true;
        }
    }
}
