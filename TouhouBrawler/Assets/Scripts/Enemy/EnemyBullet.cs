using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    private Enemy enemy;
    private new Collider damageCollider;
    private Rigidbody rb;
    public float returnTimerMax = 4f;
    private float returnTimer;
    bool ready = false;

    public void GetInit(Enemy enemy)
    {
        this.enemy = enemy;

        damageCollider = GetComponent<Collider>();
        damageCollider.isTrigger = true;
        rb = GetComponent<Rigidbody>();


        this.gameObject.layer = LayerMask.NameToLayer("Damage Collider");
        ready = true;
    }

    public void FireProjectile()
    {
        rb.velocity = enemy.shootPosition.forward * enemy.fireVelocity;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<PlayerHealth>() && other.gameObject.layer == LayerMask.NameToLayer("Player")
            && ready)
        {
            other.gameObject.GetComponent<PlayerHealth>().Damage(enemy.damageOutput);
            Destroy(this.gameObject);
        }
    }

    private void Update()
    {
        ReturnTimer();
    }

    void ReturnTimer()
    {
        if (this.gameObject.activeInHierarchy)
        {
            returnTimer += Time.deltaTime;

            if (returnTimer >= returnTimerMax)
            {
                Destroy(this.gameObject);
            }
        }
    }
}
