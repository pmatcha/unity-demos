using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyEvents : MonoBehaviour
{
    Enemy e;

    // Start is called before the first frame update
    void Start()
    {
        e = GetComponentInParent<Enemy>();
    }

    public void FireProjectile()
    {
        e.FireProjectile();
    }

    public void CloseAttack()
    {
        e.CloseAttack();
    }
}
