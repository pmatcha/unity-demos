using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    [Header("References")]
    public int health;
    public int healthMax = 5;
    public int damageOutput = 2;
    public float destroyTime = 2f;
    public float attackCooldown = 2f;
    public float iFramesTime = 0.1f;
    public float distanceToShoot = 4f;
    //public float distanceToDistance = 7f;
    public float chargeTime = 3f;
    public float fireVelocity = 20f;
    public GameObject healthPickup;
    public GameObject bullet;
    public float moveSpeed = 4f;
    public float rotationSpeed = 5f;
    public Transform shootPosition;

    [Header("States")]
    public bool isDead;
    public bool isAttacking;
    public bool isCharging;
    public bool isAttackCooldown;
    public bool canAttackCooldown;
    public bool iFrames;

    private float _attackCooldown;
    private float _charge;
    private float _destroy;
    private float _iframes;
    private Animator _aniamtor;
    private NavMeshAgent _agent;
    private Vector3 targetDestination;
    private Vector3 directionToTarget;

    // Start is called before the first frame update
    void Start()
    {
        health = healthMax;

        _aniamtor = GetComponentInChildren<Animator>();
        _agent = GetComponent<NavMeshAgent>();

        isAttackCooldown = true;
        //SetDestination(PlayerHealth.Instance.transform.position);

        this.gameObject.layer = LayerMask.NameToLayer("Enemy");
    }

    // Update is called once per frame
    void Update()
    {
        iFramesUpdate();
        IsDeadUpdate();
        Distance();
        LookAtTarget();
        ChargeAttack();
        AttackCooldown();
        AnimatorValues();
    }

    void AnimatorValues()
    {
        float square = _agent.desiredVelocity.sqrMagnitude;

        float v = Mathf.Clamp(square, 0, .5f);

        _aniamtor.SetFloat("Vertical", v, 0.2f, Time.deltaTime);

        _aniamtor.SetBool("IsCharging", isCharging);
        _aniamtor.SetBool("IsDead", isDead);
    }

    void LookAtTarget()
    {
        if ((DistanceFromPlayer() < distanceToShoot) && !isAttacking)
        {
            directionToTarget = PlayerHealth.Instance.transform.position - transform.position;
            Vector3 dir = directionToTarget;
            dir.y = 0;
            if (dir == Vector3.zero)
                dir = transform.forward;

            Quaternion targetRotation = Quaternion.LookRotation(dir);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * rotationSpeed);
        }
    }

    public void StartAttack()
    {
        isCharging = false;

        StopAgent();

        _aniamtor.Play("Fire");

        //Sound

        isAttacking = true;
    }

    public void CloseAttack()
    {
        isAttacking = false;
        isAttackCooldown = true;
    }

    public void FireProjectile()
    {
        //Play Sound

        EnemyBullet b = Instantiate(bullet, shootPosition.position, Quaternion.identity).GetComponent<EnemyBullet>();
        b.GetInit(this);
        b.gameObject.SetActive(true);
        b.FireProjectile();
    }

    void ChargeAttack()
    {
        if (isCharging)
        {
            if (_charge < chargeTime)
                _charge += Time.deltaTime;
            else
            {
                // Start Attack
                _charge = 0;
                StartAttack();
            }
        }
    }

    void Distance()
    {
        if (isDead)
            return;

        if (DistanceFromPlayer() < distanceToShoot && !isAttackCooldown && !isAttacking)
        {
            StopAgent();
            isCharging = true;
        }
    }

    float DistanceFromPlayer()
    {
        float r = Vector3.Distance(PlayerHealth.Instance.transform.position, this.transform.position);
        return r;
    }

    void AttackCooldown()
    {
        if(isAttackCooldown)
        {
            if (_attackCooldown < attackCooldown)
                _attackCooldown += Time.deltaTime;
            else
            {
                _attackCooldown = 0;
                isAttackCooldown = false;
            }
        }
    }

    void iFramesUpdate()
    {
        if (iFrames)
        {
            if (_iframes < iFramesTime)
                _iframes += Time.deltaTime;
            else
            {
                _iframes = 0;

                canAttackCooldown = true;
                iFrames = false;
            }
        }
    }

    public void TakeDamage(int health)
    {
        if (iFrames)
            return;

        if (this.health > 0)
        {
            _aniamtor.Play("Damage");
            this.health -= health;
        }
        else
        {
            this.health = 0;
            isDead = true;
        }
    }

    void IsDeadUpdate()
    {
        if(isDead)
        {
            StopAgent();

            if (_destroy < destroyTime)
                _destroy += Time.deltaTime;
            else
            {
                int ran = Random.Range(0, 100);
                float chance = 50f;

                if (PlayerHealth.Instance.health < 3)
                    chance = 50f + 35f;
                else
                    chance = 50f;

                if (ran > chance)
                {
                    GameObject hpu = Instantiate(healthPickup);
                    hpu.transform.position = this.transform.position;

                    EnemiesInScene.Instance.RemoveEnemy(this);

                    //Death Particles
                    //Drop Pickup Sound

                    Destroy(this.gameObject);
                }
                else
                {
                    Debug.Log(ran);

                    //Death Particles

                    EnemiesInScene.Instance.RemoveEnemy(this);
                    Destroy(this.gameObject);
                }
            }
        }
    }

    void SetDestination(Vector3 d)
    {
        _agent.SetDestination(d);
    }

    void GoToDestination()
    {
        _agent.SetDestination(PlayerHealth.Instance.transform.position);
    }

    void ResumeAgent()
    {
        _agent.isStopped = false;

    }

    void StopAgent()
    {
        _agent.isStopped = true;
    }
}
