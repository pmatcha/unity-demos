using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesInScene : MonoBehaviour
{
    Enemy[] enemiesInitLoad;
    public GameObject enemyPrefab;
    public List<Enemy> enemies;

    public static EnemiesInScene Instance;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        enemiesInitLoad = FindObjectsOfType<Enemy>();

        foreach(Enemy e in enemiesInitLoad)
        {
            enemies.Add(e);
        }
    }

    private void Update()
    {
       
    }

    public void AddEnemy(Enemy e)
    {
        enemies.Add(e);
    }

    public void RemoveEnemy(Enemy e)
    {
        enemies.Remove(e);
    }

    public void SpawnEnemy()
    {
        Enemy enemy = Instantiate(enemyPrefab, Vector3.zero, Quaternion.identity).GetComponent<Enemy>();
        AddEnemy(enemy);
    }
}
