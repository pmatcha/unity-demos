using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<PlayerHealth>())
        {
            MySceneManager.Instance.checkpoint = this.transform.position;

            if (!MySceneManager.Instance.hitCheckpoint)
                MySceneManager.Instance.hitCheckpoint = true;
        }
    }
}
