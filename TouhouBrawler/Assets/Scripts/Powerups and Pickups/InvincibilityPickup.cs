using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvincibilityPickup : MonoBehaviour
{
    private bool giveAttackUp;

    public void AddPowerUp()
    {
        if (!giveAttackUp)
        {
            PlayerHealth.Instance.StartInvincibility();
            TutorialChecker.Instance.InvPowerup();
            //AudioManager.Instance.Play("health_pickup");
            Destroy(this.gameObject);
            giveAttackUp = true;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<PlayerHealth>())
        {
            AddPowerUp();
        }
    }
}
