using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackUp : MonoBehaviour
{
    private bool giveAttackUp;

    public void AddPowerUp()
    {
        if (!giveAttackUp)
        {
            PlayerAttack.Instance.AttackUpStart();
            TutorialChecker.Instance.AttackPowerup();
            //AudioManager.Instance.Play("health_pickup");
            Destroy(this.gameObject);
            giveAttackUp = true;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<PlayerAttack>())
        {
            AddPowerUp();
        }
    }
}
