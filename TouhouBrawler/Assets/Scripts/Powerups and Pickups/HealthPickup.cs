﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickup : MonoBehaviour
{
    public int health = 3;
    private bool giveHealth;

    public void AddHealth()
    {
        if (!giveHealth)
        {
            PlayerHealth.Instance.AddHealth(health);
            TutorialChecker.Instance.HealthPickup();
            //AudioManager.Instance.Play("health_pickup");
            Destroy(this.gameObject);
            giveHealth = true;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.GetComponent<PlayerHealth>())
        {
            AddHealth();
        }
    }
}
