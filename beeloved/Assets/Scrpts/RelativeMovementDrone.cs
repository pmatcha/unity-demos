﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class RelativeMovementDrone : MonoBehaviour
{
    public float jumpSpeed = 90.0f;
    public float gravity = -9.8f;
    public float terminalVelocity = -10.0f;
    public float minFall = -1.5f;
    public float rotSpeed = 15.0f;
    public float moveSpeed = 6.0f;
    private float _vertSpeed;
    public float pushForce = 3.0f;
    public float distanceToGround = 1.5f;
    public float baseJumpMultiplier = 1;

    [SerializeField] private AudioSource soundSource;
    [SerializeField] private AudioClip jumpSound;

    [SerializeField] private Transform target;
    private CharacterController _charController;
    private ControllerColliderHit _contact;
    private Animator _animator;

    private float lastHorInput = 0;
    private float lastVerInput = 0;
    private bool dontRotate = false;
    private bool chargingJump = false;
    private bool bufferJump = false;
    private float jumpMultiplier;

    // Start is called before the first frame update
    void Start()
    {
        _charController = GetComponent<CharacterController>();
        _vertSpeed = minFall;
        _animator = GetComponent<Animator>();
        jumpMultiplier = baseJumpMultiplier*0.25f;
    }

    
    // Update is called once per frame
    void Update()
    {
        Vector3 movement = Vector3.zero;

        float horInput = 0;
        float vertInput = 0;

        bool hitGround = false;
        RaycastHit hit;
        
        //GROUND COLLISION CONTROL
        if (_vertSpeed < 0 && Physics.Raycast(transform.position, Vector3.down, out hit))
        {
            float check = distanceToGround;
            hitGround = hit.distance <= check;
        }
    
        //STATE MOVEMENT CONTROL
        if (hitGround)
        {
            horInput = Input.GetAxis("Horizontal");
            vertInput = Input.GetAxis("Vertical");
            lastHorInput = horInput;
            lastVerInput = vertInput;
            dontRotate = false;
            if (chargingJump)
            {
                dontRotate = true;
            }
        }
        else
        {
            horInput = lastHorInput;
            vertInput = lastVerInput;
            dontRotate = true;
            
        }

        //MOVEMENT AND ROTATION
        if (horInput != 0 || vertInput != 0)
        {
            movement.x = horInput * moveSpeed;
            movement.z = vertInput * moveSpeed;
            movement = Vector3.ClampMagnitude(movement, moveSpeed);

            if(dontRotate == false)
            {
                Quaternion tmp = target.rotation;
                target.eulerAngles = new Vector3(0, target.eulerAngles.y, 0);
                movement = target.TransformDirection(movement);
                target.rotation = tmp;
                Quaternion direction = Quaternion.LookRotation(movement);
                transform.rotation = Quaternion.Lerp(transform.rotation, direction, rotSpeed * Time.deltaTime);
            }
            else if (chargingJump)
            {
                movement.x = 0 * moveSpeed;
                movement.z = 1 * moveSpeed;
                movement = Vector3.ClampMagnitude(movement, moveSpeed);
                movement = transform.TransformDirection(movement);
                }

            else
            {
                movement.x = 0 * moveSpeed;
                movement.z = 1 * moveSpeed;
                movement = transform.TransformDirection(movement);
            }
        }
        _animator.SetFloat("Speed", movement.sqrMagnitude);


        //JUMP MECHANICS
        if (hitGround)
        {
            _animator.SetBool("Falling", false);
            if (Input.GetButtonDown("Jump") || bufferJump)
            {
                chargingJump = true;
                _animator.SetBool("chargingJump", true);
                bufferJump = false;
            }
            else if (Input.GetButtonUp("Jump"))
            {
                chargingJump = false;
                _vertSpeed = jumpSpeed*jumpMultiplier;
                jumpMultiplier = baseJumpMultiplier*0.25f;
                
                _animator.SetBool("chargingJump", false);
                _animator.SetBool("Jumping", true);
                
                AudioSource.PlayClipAtPoint(this.GetComponent<AudioSource>().clip, target.position);
            }
            else
            {
                _vertSpeed = minFall;
                _animator.SetBool("Jumping", false);

                if(chargingJump)
                {
                    jumpMultiplier = Mathf.Clamp(jumpMultiplier + (1f *Time.deltaTime), baseJumpMultiplier*0.25f, baseJumpMultiplier);
                    Debug.Log("Jump multiplier: "+ jumpMultiplier);
                }

            }
            _animator.SetFloat("VertSpeed", _vertSpeed);
        }
        else
        {
            if (Input.GetButtonDown("Jump"))
            {
                bufferJump = true;
            }
            else if (Input.GetButtonUp("Jump"))
            {
                bufferJump = false;
            }

            _vertSpeed += gravity * 5 * Time.deltaTime;
            if (_vertSpeed < terminalVelocity) {
                _vertSpeed = terminalVelocity;
            }

            if (_contact != null ) {
                _animator.SetBool("Falling", true);
            }

            if (_charController.isGrounded)
            {
                _animator.SetBool("Jumping", false);
                if (Vector3.Dot(movement, _contact.normal) < 0)
                {
                    movement = _contact.normal * moveSpeed;
                }
                else
                {
                    movement += _contact.normal * moveSpeed;
                }
                    
            
            }
            _animator.SetFloat("VertSpeed", _vertSpeed);
        }
        
        //APPLY MOVEMENT
        movement.y = _vertSpeed;
        
        movement *= Time.deltaTime;

        if (!_animator.GetCurrentAnimatorClipInfo(0)[0].clip.name.Contains("Attack") && !chargingJump)
        {
            _charController.Move(movement);
        }
        else if (chargingJump)
        {
            _charController.Move(movement*0.00001f);
        }
        else
        {
            _charController.Move(movement*0.1f);
        }
 
    }

    //JUMP MECHANICS
    void OnControllerColliderHit(ControllerColliderHit hit) {
        _contact = hit;

        
        Vector3 directionOfCollision = _contact.point - transform.position;

        //Debug.Log("Colliding vector is " + directionOfCollision);
        if (directionOfCollision.y > 1f || directionOfCollision.y < -1f)
        {
            if(directionOfCollision.y > 2.5f)
            {
                _vertSpeed = minFall;
            }
            else
            {
                transform.RotateAround(transform.position, transform.up, 180f);
                Debug.Log("<color=blue>HitfromAngle! </color>" + directionOfCollision.y);
            }
        }

        Rigidbody body = hit.collider.attachedRigidbody;
        if (body != null && !body.isKinematic)
        {
            body.velocity = hit.moveDirection * pushForce;
        }
    }
    
}