﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlowingMushroom : MonoBehaviour
{

    public float glowingspeed = 10f;

    float base_glowingvalue_r = 0.5f;
    float base_glowingvalue_g = 0.5f;
    float base_glowingvalue_b = 0.5f;
    Color glowingvalue;
    
    bool decreasing = true;
    public float minvalue = 0.3f;
    public float maxvalue = 0.5f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (base_glowingvalue_r < minvalue)
        {
            decreasing = false;    
        }
        
        if (base_glowingvalue_r > maxvalue)
        {
            decreasing = true;    
        }
        
        if (decreasing)
        {
            base_glowingvalue_r -= 0.01f * Time.deltaTime * glowingspeed;
            base_glowingvalue_g -= 0.01f * Time.deltaTime * glowingspeed;
            base_glowingvalue_b -= 0.01f * Time.deltaTime * glowingspeed;
        }
        else
        {
            base_glowingvalue_r += 0.01f * Time.deltaTime * glowingspeed;
            base_glowingvalue_g += 0.01f * Time.deltaTime * glowingspeed;
            base_glowingvalue_b += 0.01f * Time.deltaTime * glowingspeed;
        }
        
        glowingvalue = new Color (base_glowingvalue_r, base_glowingvalue_g, base_glowingvalue_b, 1f);

        GetComponent<Renderer>().material.color = glowingvalue;
    }
}
