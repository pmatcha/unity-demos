﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitCamera : MonoBehaviour
{
    [SerializeField] private Transform target;

    public float rotSpeed = 1.5f;
    private float _rotY;
    private float _rotX;
    private float _previousRotX;
    private Vector3 _offset;
    private Vector3 _tempOffset;
    public float MaxTargetCameraDiffY;
    // Start is called before the first frame update
    void Start()
    {
        _rotY = transform.eulerAngles.y;
        _rotX = 0;
        _offset = target.position - transform.position;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("escape") && Cursor.visible)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else if (Input.GetKeyDown("escape") && !Cursor.visible)
        {   
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    void LateUpdate()
    {
        float horInput = Input.GetAxis("Horizontal");
        //if (horInput != 0)
        //{
            //_rotY += horInput * rotSpeed;
        //}
        //else
        //{
            _rotY += Input.GetAxis("Mouse X") * rotSpeed * 2;
            _previousRotX = _rotX;
            _rotX += Input.GetAxis("Mouse Y") * rotSpeed * 2;
        //}
        
        Quaternion rotation = Quaternion.Euler(-_rotX, _rotY, 0);
        float targetCameraDiffY = (((target.position - (rotation * _offset)).y) - (target.position.y));
        if (targetCameraDiffY > MaxTargetCameraDiffY || targetCameraDiffY < -MaxTargetCameraDiffY)
        {
            _rotX = _previousRotX;
            rotation = Quaternion.Euler(-_rotX, _rotY, 0);
        }

        //Debug.Log(targetCameraDiffY);

        if (targetCameraDiffY < -0)
        {
            _tempOffset = _offset.normalized;
            //Debug.Log("Offset: " + _offset);
            //Debug.Log("Offset Magnitude: " + _offset.magnitude);
            //Debug.Log("Offset Normalized: " + _offset.normalized);
            //Debug.Log("New Magnitude: " + (((_offset.magnitude)/-targetCameraDiffY)));
            float _tempOffsetMagnitude = Mathf.Clamp(((_offset.magnitude)/-(targetCameraDiffY)), 1f, _offset.magnitude);
            _tempOffset = _tempOffset * _tempOffsetMagnitude;
        }
        else
        {
            _tempOffset = _offset;
        }
        
        //Debug.Log(target.GetComponentInParent<CharacterController>().radius);
        transform.position = target.position - (rotation * _tempOffset);
        transform.LookAt(target);
    }
}
